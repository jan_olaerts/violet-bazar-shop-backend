package be.janolaerts.violetbazarshop.exception.domain;

public class DniAlreadyExistsException extends Exception {

    public DniAlreadyExistsException(String message) {
        super(message);
    }
}