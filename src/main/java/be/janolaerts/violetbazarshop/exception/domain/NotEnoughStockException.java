package be.janolaerts.violetbazarshop.exception.domain;

public class NotEnoughStockException extends Exception {

    public NotEnoughStockException(String message) {
        super(message);
    }
}