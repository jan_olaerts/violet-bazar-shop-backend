package be.janolaerts.violetbazarshop.exception.domain;

public class ImageException extends RuntimeException {

    public ImageException(Throwable cause) {
        super(cause);
    }
}