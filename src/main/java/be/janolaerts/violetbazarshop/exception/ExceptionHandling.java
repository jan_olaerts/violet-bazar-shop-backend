package be.janolaerts.violetbazarshop.exception;

import be.janolaerts.violetbazarshop.domain.HttpResponse;
import be.janolaerts.violetbazarshop.exception.domain.*;
import com.auth0.jwt.exceptions.TokenExpiredException;
import com.mercadopago.exceptions.MPException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.LockedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.persistence.NoResultException;
import java.nio.file.AccessDeniedException;
import java.util.Objects;

import static org.springframework.http.HttpStatus.*;

public class ExceptionHandling {

    private static final Logger LOGGER = LoggerFactory.getLogger(ExceptionHandling.class);
    private static final String ACCOUNT_LOCKED = "Tu cuenta fue bloqueda";
    private static final String METHOD_IS_NOT_ALLOWED = "This request method is not allowed on this endpoint. Please send a '%s' request";
    private static final String INTERNAL_SERVER_ERROR_MESSAGE = "Inténtalo de nuevo";
    private static final String INCORRECT_CREDENTIALS = "Email / password no existe";
    private static final String ACCOUNT_DISABLED = "Tu cuenta fue deshabilitada";
    private static final String NOT_ENOUGH_STOCK = "No hay suficiente stock";
    private static final String PAYMENT_NOT_SUCCESSFUL = "El pago no fue exitoso";

    @ExceptionHandler(DisabledException.class)
    public ResponseEntity<HttpResponse> accountDisabledException(DisabledException exception) {
        LOGGER.error(exception.getMessage());
        exception.printStackTrace();
        return createHttpResponse(BAD_REQUEST, ACCOUNT_DISABLED);
    }

    @ExceptionHandler(BadCredentialsException.class)
    public ResponseEntity<HttpResponse> badCredentialsException(BadCredentialsException exception) {
        LOGGER.error(exception.getMessage());
        exception.printStackTrace();
        return createHttpResponse(BAD_REQUEST, INCORRECT_CREDENTIALS);
    }

    @ExceptionHandler(AccessDeniedException.class)
    public ResponseEntity<HttpResponse> accessDeniedException(AccessDeniedException exception) {
        LOGGER.error(exception.getMessage());
        exception.printStackTrace();
        return createHttpResponse(FORBIDDEN, INCORRECT_CREDENTIALS);
    }

    @ExceptionHandler(LockedException.class)
    public ResponseEntity<HttpResponse> lockedException(LockedException exception) {
        LOGGER.error(exception.getMessage());
        exception.printStackTrace();
        return createHttpResponse(UNAUTHORIZED, ACCOUNT_LOCKED);
    }

    @ExceptionHandler(TokenExpiredException.class)
    public ResponseEntity<HttpResponse> tokenExpiredException(TokenExpiredException exception) {
        LOGGER.error(exception.getMessage());
        exception.printStackTrace();
        return createHttpResponse(UNAUTHORIZED, exception.getMessage());
    }

    @ExceptionHandler(EmailAlreadyExistsException.class)
    public ResponseEntity<HttpResponse> emailAlreadyExistsException(EmailAlreadyExistsException exception) {
        LOGGER.error(exception.getMessage());
        exception.printStackTrace();
        return createHttpResponse(BAD_REQUEST, exception.getMessage());
    }

    @ExceptionHandler(EmailNotFoundException.class)
    public ResponseEntity<HttpResponse> emailNotFoundException(EmailNotFoundException exception) {
        LOGGER.error(exception.getMessage());
        exception.printStackTrace();
        return createHttpResponse(BAD_REQUEST, exception.getMessage());
    }

    @ExceptionHandler(DniAlreadyExistsException.class)
    public ResponseEntity<HttpResponse> dniAlreadyExistsException(DniAlreadyExistsException exception) {
        LOGGER.error(exception.getMessage());
        exception.printStackTrace();
        return createHttpResponse(BAD_REQUEST, exception.getMessage());
    }

    @ExceptionHandler(UserNotFoundException.class)
    public ResponseEntity<HttpResponse> userNotFoundException(UserNotFoundException exception) {
        LOGGER.error(exception.getMessage());
        exception.printStackTrace();
        return createHttpResponse(BAD_REQUEST, exception.getMessage());
    }

    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    public ResponseEntity<HttpResponse> methodNotSupportedException(HttpRequestMethodNotSupportedException exception) {
        LOGGER.error(exception.getMessage());
        exception.printStackTrace();
        HttpMethod supportedMethod = Objects.requireNonNull(exception.getSupportedHttpMethods()).iterator().next();
        return createHttpResponse(METHOD_NOT_ALLOWED, String.format(METHOD_IS_NOT_ALLOWED, supportedMethod));
    }

    @ExceptionHandler(NoResultException.class)
    public ResponseEntity<HttpResponse> notFoundException(NoResultException exception) {
        LOGGER.error(exception.getMessage());
        exception.printStackTrace();
        return createHttpResponse(NOT_FOUND, exception.getMessage());
    }

    @ExceptionHandler(NotEnoughStockException.class)
    public ResponseEntity<HttpResponse> notEnoughStockException(Exception exception) {
        LOGGER.error(exception.getMessage());
        exception.printStackTrace();
        return createHttpResponse(BAD_REQUEST, NOT_ENOUGH_STOCK);
    }

    @ExceptionHandler(MPException.class)
    public ResponseEntity<HttpResponse> mercadoPagoException(Exception exception) {
        LOGGER.error(exception.getMessage());
        exception.printStackTrace();
        return createHttpResponse(BAD_REQUEST, PAYMENT_NOT_SUCCESSFUL);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<HttpResponse> internalServerErrorException(Exception exception) {
        LOGGER.error(exception.getMessage());
        exception.printStackTrace();
        return createHttpResponse(INTERNAL_SERVER_ERROR, INTERNAL_SERVER_ERROR_MESSAGE);
    }

    private ResponseEntity<HttpResponse> createHttpResponse(HttpStatus httpStatus, String message) {

        HttpResponse httpResponse = new HttpResponse(httpStatus.value(), httpStatus, httpStatus.getReasonPhrase(), message);
        return new ResponseEntity<>(httpResponse, httpStatus);
    }

    @RequestMapping("/error")
    public ResponseEntity<HttpResponse> notFound404() {
        return createHttpResponse(NOT_FOUND, "There is no mapping for this URL");
    }
}