package be.janolaerts.violetbazarshop.entity;

import be.janolaerts.violetbazarshop.tools.StringTool;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Entity
@Table(name="addresses")
@EqualsAndHashCode(exclude={"id"})
public class Address implements Serializable {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Min(0)
    @Column(name="id")
    private int id;

    @NotBlank
    @Size(min=2)
    @Column(name="street", nullable=false)
    private String street;

    @NotBlank
    @Size(min=2)
    @Column(name="number", nullable=false)
    private String number;

    @NotBlank
    @Size(min=2)
    @Column(name="city", nullable=false)
    private String city;

    @NotBlank
    @Size(min=2)
    @Column(name="province", nullable=false)
    private String province;

    @NotBlank
    @Size(min=2)
    @Column(name="department", nullable=false)
    private String department;

    public Address() {
    }

    public Address(@Min(0) int id,
                   @NotBlank @Size(min = 2) String street,
                   @NotBlank @Size(min = 2) String number,
                   @NotBlank @Size(min = 2) String city,
                   @NotBlank @Size(min = 2) String province,
                   @NotBlank @Size(min = 2) String department) {

        setId(id);
        setStreet(street);
        setNumber(number);
        setCity(city);
        setProvince(province);
        setDepartment(department);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        if(id < 0) throw new IllegalArgumentException("id cannot be negative");

        this.id = id;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        if(street == null) throw new IllegalArgumentException("street cannot be null");
        if(street.length() < 2) throw new IllegalArgumentException("street must have at least 2 characters");

        this.street = StringTool.capitalizeFirstLetters(street);
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        if(number == null) throw new IllegalArgumentException("number cannot be null");
        if(number.length() < 2) throw new IllegalArgumentException("number must have at least 2 characters");

        this.number = StringTool.capitalizeFirstLetters(number);
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        if(city == null) throw new IllegalArgumentException("city cannot be null");
        if(city.length() < 2) throw new IllegalArgumentException("city must have at least 2 characters");

        this.city = StringTool.capitalizeFirstLetters(city);
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        if(province == null) throw new IllegalArgumentException("province cannot be null");
        if(province.length() < 2) throw new IllegalArgumentException("province must have at least 2 characters");

        this.province = StringTool.capitalizeFirstLetters(province);
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        if(department == null) throw new IllegalArgumentException("department cannot be null");
        if(department.length() < 2) throw new IllegalArgumentException("department must have at least 2 characters");

        this.department = StringTool.capitalizeFirstLetters(department);
    }

    @Override
    public String toString() {
        return "Address{" +
                "id=" + id +
                ", street='" + street + '\'' +
                ", number='" + number + '\'' +
                ", city='" + city + '\'' +
                ", department='" + department + '\'' +
                ", province='" + province + '\'' +
                '}';
    }
}