package be.janolaerts.violetbazarshop.entity;

import be.janolaerts.violetbazarshop.tools.StringTool;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.checkerframework.common.aliasing.qual.Unique;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Entity
@Table(name="users")
public class User implements Serializable {

    @Id
    @Unique
    @NotBlank
    @Column(name="email", nullable=false)
    private String email;

    @NotBlank
    @Size(min=2)
    @Column(name="password", nullable=false)
    @JsonProperty(access=JsonProperty.Access.WRITE_ONLY)
    private String password;

    @NotBlank
    @Size(min=8, max=8)
    @Column(name="dni", nullable=false)
    private String dni;

    @NotBlank
    @Size(min=2)
    @Column(name="firstNames", nullable=false)
    private String firstNames;

    @NotBlank
    @Size(min=2)
    @Column(name="lastName_1", nullable=false)
    private String lastName_1;

    @NotBlank
    @Size(min=2)
    @Column(name="lastName_2", nullable=false)
    private String lastName_2;

    @OneToOne(cascade=CascadeType.ALL)
    @JoinColumn(name="address_id")
    private Address address;

    @Column(name="lastLoginDate")
    private Date lastLoginDate;

    @Column(name="lastLoginDateDisplay")
    private Date lastLoginDateDisplay;

    @Column(name="joinDate")
    private Date joinDate;

    @Column(name="role")
    private String role;

    @Column(name="authorities")
    private String[] authorities;

    @Column(name="isNotLocked")
    private boolean isNotLocked;

    @Column(name="isActive")
    private boolean isActive;

    @OneToMany(mappedBy="user", fetch=FetchType.LAZY, cascade=CascadeType.REMOVE)
    private List<Order> orders;

    public User() {
    }

    public User(@Unique @NotBlank String email,
                @NotBlank @Size(min=2) String password,
                @NotBlank @Size(min=8, max=8) String dni,
                @NotBlank @Size(min=2) String firstNames,
                @NotBlank @Size(min=2) String lastName_1,
                @NotBlank @Size(min=2) String lastName_2,
                Address address,
                Date lastLoginDate,
                Date lastLoginDateDisplay,
                Date joinDate,
                String role,
                String[] authorities,
                boolean isNotLocked,
                boolean isActive,
                List<Order> orders) {

        setEmail(email);
        setPassword(password);
        setDni(dni);
        setFirstNames(firstNames);
        setLastName_1(lastName_1);
        setLastName_2(lastName_2);
        setAddress(address);
        setLastLoginDate(lastLoginDate);
        setLastLoginDateDisplay(lastLoginDateDisplay);
        setJoinDate(joinDate);
        setRole(role);
        setAuthorities(authorities);
        setNotLocked(isNotLocked);
        setActive(isActive);
        setOrders(orders);
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        if(email == null) throw new IllegalArgumentException("email cannot be null");
        if(!StringTool.validateEmail(email)) throw new IllegalArgumentException("email is not valid");

        this.email = email.toLowerCase();
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        if(password == null) throw new IllegalArgumentException("password cannot be null");
        if(password.length() < 2) throw new IllegalArgumentException("password must have at least 2 characters");

        this.password = password;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        if(dni == null) throw new IllegalArgumentException("dni cannot be null");
        if(!dni.matches("^\\d{8}$")) throw new IllegalArgumentException("dni must consist of 8 numbers");

        this.dni = dni;
    }

    public String getFirstNames() {
        return firstNames;
    }

    public void setFirstNames(String firstNames) {
        if(firstNames == null) throw new IllegalArgumentException("firstNames cannot be null");
        if(firstNames.length() < 2) throw new IllegalArgumentException("firstNames must have at least 2 characters");

        this.firstNames = StringTool.capitalizeFirstLetters(firstNames);
    }

    public String getLastName_1() {
        return lastName_1;
    }

    public void setLastName_1(String lastName_1) {
        if(lastName_1 == null) throw new IllegalArgumentException("lastName_1 cannot be null");
        if(lastName_1.length() < 2) throw new IllegalArgumentException("lastName_1 must have at least 2 characters");

        this.lastName_1 = StringTool.capitalizeFirstLetters(lastName_1);
    }

    public String getLastName_2() {
        return lastName_2;
    }

    public void setLastName_2(String lastName_2) {
        if(lastName_2 == null) throw new IllegalArgumentException("lastName_2 cannot be null");
        if(lastName_2.length() < 2) throw new IllegalArgumentException("lastName_2 must have at least 2 characters");

        this.lastName_2 = StringTool.capitalizeFirstLetters(lastName_2);
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        if(address == null) throw new IllegalArgumentException("address cannot be null");

        this.address = address;
    }

    public Date getLastLoginDate() {
        return lastLoginDate;
    }

    public void setLastLoginDate(Date lastLoginDate) {
        this.lastLoginDate = lastLoginDate;
    }

    public Date getLastLoginDateDisplay() {
        return lastLoginDateDisplay;
    }

    public void setLastLoginDateDisplay(Date lastLoginDateDisplay) {
        this.lastLoginDateDisplay = lastLoginDateDisplay;
    }

    public Date getJoinDate() {
        return joinDate;
    }

    public void setJoinDate(Date joinDate) {
        if(joinDate.after(new Date())) throw new IllegalArgumentException("joinDate cannot be in the future");

        this.joinDate = joinDate;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String[] getAuthorities() {
        return authorities;
    }

    public void setAuthorities(String[] authorities) {
        this.authorities = authorities;
    }

    public boolean isNotLocked() {
        return isNotLocked;
    }

    public void setNotLocked(boolean notLocked) {
        isNotLocked = notLocked;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public List<Order> getOrders() {
        return orders;
    }

    public void setOrders(List<Order> orders) {
        if(orders == null) throw new IllegalArgumentException("orders cannot be null");

        this.orders = orders;
    }

    @Override
    public String toString() {
        return "User{" +
                "email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", dni='" + dni + '\'' +
                ", firstNames='" + firstNames + '\'' +
                ", lastName_1='" + lastName_1 + '\'' +
                ", lastName_2='" + lastName_2 + '\'' +
                ", address=" + address +
                ", lastLoginDate=" + lastLoginDate +
                ", lastLoginDateDisplay=" + lastLoginDateDisplay +
                ", joinDate=" + joinDate +
                ", role='" + role + '\'' +
                ", authorities=" + Arrays.toString(authorities) +
                ", isNotLocked=" + isNotLocked +
                ", isActive=" + isActive +
                ", orders=" + orders +
                '}';
    }
}