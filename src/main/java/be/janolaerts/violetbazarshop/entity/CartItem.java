package be.janolaerts.violetbazarshop.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Table(name="cart_items")
public class CartItem implements Serializable {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Min(0)
    @Column(name="id")
    private int id;

    @Size(min=2)
    @Column(name="name", nullable=false)
    private String name;

    @Size(min=2)
    @Column(name="image_url", nullable=false)
    private String imageUrl;

    @Min(0)
    @Column(name="price", nullable=false)
    private BigDecimal price;

    @Min(0)
    @Column(name="stock", nullable=false)
    private int stock;

    @Min(1)
    @Column(name="quantity", nullable=false)
    private int quantity;

    @ManyToOne
    @JoinColumn(name="order_id")
    @JsonIgnore
    private Order order;

    public CartItem() {
    }

    public CartItem(Product product, @Min(1) int quantity, Order order) {
        setId(product.getId());
        setName(product.getName());
        setImageUrl(product.getImageUrl());
        setPrice(product.getPrice());
        setStock(product.getStock());
        setQuantity(quantity);
        setOrder(order);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        if(id < 0) throw new IllegalArgumentException("id cannot be negative");

        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        if(name == null) throw new IllegalArgumentException("name cannot be null");
        if(name.length() < 2) throw new IllegalArgumentException("name must have at least 2 characters");

        this.name = name;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        if(imageUrl == null) throw new IllegalArgumentException("imageUrl cannot be null");

        this.imageUrl = imageUrl;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        if(price == null) throw new IllegalArgumentException("price cannot be null");
        if(price.doubleValue() < 0) throw new IllegalArgumentException("price cannot be smaller than 0");

        this.price = price;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        if(stock < 0) throw new IllegalArgumentException("stock cannot be smaller than 0");

        this.stock = stock;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        if(quantity < 1) throw new IllegalArgumentException("quantity cannot be 0 or negative");

        this.quantity = quantity;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        if(order == null) throw new IllegalArgumentException("order cannot be null");

        this.order = order;
    }

    @Override
    public String toString() {
        return "CartItem{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", imageUrl='" + imageUrl + '\'' +
                ", price=" + price +
                ", stock=" + stock +
                ", quantity=" + quantity +
                ", order=" + order +
                '}';
    }
}