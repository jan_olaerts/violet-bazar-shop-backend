package be.janolaerts.violetbazarshop.entity;

import be.janolaerts.violetbazarshop.tools.StringTool;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Table(name="orders")
public class Order implements Serializable {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name="id")
    private int id;

    @Column(name="placement_time", nullable=false)
    private LocalDateTime placementTime;

    @ManyToOne
    @JoinColumn(name="user_email")
    @JsonBackReference
    private User user;

    @NotBlank
    @Size(min=2)
    @Column(name="send_firstNames", nullable=false)
    private String send_firstNames;

    @NotBlank
    @Size(min=2)
    @Column(name="send_lastNames", nullable=false)
    private String send_lastNames;

    @OneToOne(cascade=CascadeType.DETACH)
    @JoinColumn(name="address_id")
    private Address send_address;

    @OneToMany(mappedBy="order", fetch=FetchType.EAGER, cascade=CascadeType.DETACH)
    List<CartItem> cartItems;

    @Min(0)
    @Column(name="total_price")
    BigDecimal totalPrice;

    public Order() {
    }

    public Order(int id,
                 LocalDateTime placementTime,
                 User user,
                 @NotBlank @Size(min = 2) String send_firstNames,
                 @NotBlank @Size(min = 2) String send_lastNames,
                 Address send_address,
                 List<CartItem> cartItems,
                 BigDecimal totalPrice) {

        setId(id);
        setPlacementTime(placementTime);
        setUser(user);
        setSend_firstNames(send_firstNames);
        setSend_lastNames(send_lastNames);
        setSend_address(send_address);
        setCartItems(cartItems);
        setTotalPrice(totalPrice);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        if(id < 0) throw new IllegalArgumentException("id cannot be negative");

        this.id = id;
    }

    public LocalDateTime getPlacementTime() {
        return placementTime;
    }

    public void setPlacementTime(LocalDateTime placementTime) {
        if(placementTime == null) throw new IllegalArgumentException("placementTime cannot be null");
        if(placementTime.isAfter(LocalDateTime.now())) throw new IllegalArgumentException("placementTime cannot be in the future");

        this.placementTime = placementTime;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        if(user == null) throw new IllegalArgumentException("user cannot be null");

        this.user = user;
    }

    public String getSend_firstNames() {
        return send_firstNames;
    }

    public void setSend_firstNames(String send_firstNames) {
        if(send_firstNames == null) throw new IllegalArgumentException("send_firstNames cannot be null");
        if(send_firstNames.length() < 2) throw new IllegalArgumentException("send_firstNames must have at least 2 characters");

        this.send_firstNames = StringTool.capitalizeFirstLetters(send_firstNames);
    }

    public String getSend_lastNames() {
        return send_lastNames;
    }

    public void setSend_lastNames(String send_lastName_1) {
        if(send_lastName_1 == null) throw new IllegalArgumentException("send_lastName_1 cannot be null");
        if(send_lastName_1.length() < 2) throw new IllegalArgumentException("send_lastName_1 must have at least 2 characters");

        this.send_lastNames = StringTool.capitalizeFirstLetters(send_lastName_1);
    }

    public Address getSend_address() {
        return send_address;
    }

    public void setSend_address(Address send_address) {
        if(send_address == null) throw new IllegalArgumentException("send_address cannot be null");

        this.send_address = send_address;
    }

    public List<CartItem> getCartItems() {
        return cartItems;
    }

    public void setCartItems(List<CartItem> cartItems) {
        if(cartItems == null) throw new IllegalArgumentException("cartItems cannot be null");

        this.cartItems = cartItems;
    }

    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(BigDecimal totalPrice) {
        if(totalPrice == null) throw new IllegalArgumentException("totalPrice cannot be null");
        if(totalPrice.doubleValue() < 0) throw new IllegalArgumentException("totalPrice cannot be smaller than 0");

        this.totalPrice = totalPrice;
    }

    @Override
    public String toString() {
        return "Order{" +
                "id=" + id +
                ", placementTime=" + placementTime +
                ", user=" + user +
                ", send_firstNames='" + send_firstNames + '\'' +
                ", send_lastNames='" + send_lastNames + '\'' +
                ", send_address=" + send_address +
                ", cartItems=" + cartItems +
                ", totalPrice=" + totalPrice +
                '}';
    }
}