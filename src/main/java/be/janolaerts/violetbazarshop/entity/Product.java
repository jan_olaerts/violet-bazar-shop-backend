package be.janolaerts.violetbazarshop.entity;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Table(name="products")
public class Product implements Serializable {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Min(0)
    @Column(name="id")
    private int id;

    @NotBlank
    @Size(min=2)
    @Column(name="name", nullable=false)
    private String name;

    @NotBlank
    @Size(min=2)
    @Column(name="brand", nullable=false)
    private String brand;

    @NotBlank
    @Size(min=20)
    @Column(name="description", length = 100000)
    private String description;

    @Min(0)
    @Column(name="price")
    private BigDecimal price;

    @NotBlank
    @Column(name="image_url", nullable=false)
    private String imageUrl;

    @Min(0)
    @Column(name="stock", nullable=false)
    private int stock;

    @ManyToOne
    @JoinColumn(name="category_id")
    private ProductCategory category;

    public Product() {
    }

    public Product(@Min(0) int id,
                   @NotBlank @Size(min = 2) String name,
                   @NotBlank @Size(min = 2) String brand,
                   @NotBlank @Size(min = 20) String description,
                   @Min(0) BigDecimal price,
                   @NotBlank String imageUrl,
                   @Min(0) int stock,
                   ProductCategory category) {

        setId(id);
        setName(name);
        setBrand(brand);
        setDescription(description);
        setPrice(price);
        setImageUrl(imageUrl);
        setStock(stock);
        setCategory(category);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        if(id < 0) throw new IllegalArgumentException("id cannot be negative");

        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        if(name == null) throw new IllegalArgumentException("name cannot be null");
        if(name.length() < 2) throw new IllegalArgumentException("name must have at least 2 characters");

        this.name = name;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        if(brand == null) throw new IllegalArgumentException("brand cannot be null");
        if(brand.length() < 2) throw new IllegalArgumentException("brand must have at least 2 characters");

        this.brand = brand;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        if(description == null) throw new IllegalArgumentException("description cannot be null");
        if(description.length() < 20) throw new IllegalArgumentException("description must have at least 20 characters");

        this.description = description;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        if(price == null) throw new IllegalArgumentException("price cannot be null");
        if(price.doubleValue() < 0) throw new IllegalArgumentException("price cannot be smaller than 0");

        this.price = price;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        if(imageUrl == null) throw new IllegalArgumentException("imageUrl cannot be null");

        this.imageUrl = imageUrl;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        if(stock < 0) throw new IllegalArgumentException("stock cannot be smaller than 0");

        this.stock = stock;
    }

//    @JsonIgnore
    public ProductCategory getCategory() {
        return category;
    }

    public void setCategory(ProductCategory productCategory) {
        if(productCategory == null) throw new IllegalArgumentException("productCategory cannot be null");

        this.category = productCategory;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", brand='" + brand + '\'' +
                ", description='" + description + '\'' +
                ", price=" + price +
                ", imageUrl='" + imageUrl + '\'' +
                ", stock=" + stock +
                ", category=" + category +
                '}';
    }
}