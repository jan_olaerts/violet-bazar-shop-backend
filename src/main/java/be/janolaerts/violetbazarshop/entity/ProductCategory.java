package be.janolaerts.violetbazarshop.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name="product_categories")
public class ProductCategory implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Min(0)
    @Column(name="id")
    private int id;

    @NotBlank
    @Size(min=2)
    @Column(name="category_name", nullable=false)
    private String categoryName;

    @OneToMany(mappedBy="category", fetch=FetchType.LAZY, cascade=CascadeType.REMOVE)
    private List<Product> products;

    public ProductCategory() {
    }

    public ProductCategory(@Min(0) int id, @NotBlank @Size(min = 2) String categoryName,
                           List<Product> products) {

        setId(id);
        setCategoryName(categoryName);
        setProducts(products);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        if(id < 0) throw new IllegalArgumentException("id cannot be negative");

        this.id = id;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        if(categoryName == null) throw new IllegalArgumentException("categoryName cannot be null");
        if(categoryName.length() < 2) throw new IllegalArgumentException("categoryName must have at least 2 characters");

        this.categoryName = categoryName;
    }

    @JsonIgnore
    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        if(products == null) throw new IllegalArgumentException("products cannot be null");

        this.products = products;
    }

    @Override
    public String toString() {
        return "ProductCategory{" +
                "id=" + id +
                ", categoryName='" + categoryName + '\'' +
                ", products=" + products +
                '}';
    }
}