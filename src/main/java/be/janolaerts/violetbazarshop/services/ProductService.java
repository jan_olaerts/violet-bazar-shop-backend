package be.janolaerts.violetbazarshop.services;

import be.janolaerts.violetbazarshop.entity.CartItem;
import be.janolaerts.violetbazarshop.entity.Product;
import be.janolaerts.violetbazarshop.repository.ProductCategoryRepository;
import be.janolaerts.violetbazarshop.repository.ProductRepository;
import be.janolaerts.violetbazarshop.tools.ImageTool;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class ProductService implements ProductService_I {

    private ProductRepository productRepository;
    private ProductCategoryRepository productCategoryRepository;
    private ImageTool imageTool;
    ObjectMapper mapper;

    @Autowired
    public ProductService(ProductRepository productRepository, ProductCategoryRepository productCategoryRepository,
                          ImageTool imageTool) {

        this.productRepository = productRepository;
        this.productCategoryRepository = productCategoryRepository;
        this.imageTool = imageTool;
        this.mapper = new ObjectMapper();
    }

    @Override
    public Product getProductById(int id) {
        return productRepository.getProductById(id);
    }

    @Override
    public List<Product> getAllProducts() {
        return productRepository.getAllProductsByOrderByCategoryAsc();
    }

    @Override
    public List<Product> getAllProductsByProductCategoryId(int productCategoryId) {
        return productRepository.getAllProductsByCategory_IdOrderByName(productCategoryId);
    }

    @Override
    public List<Product> getAllProductsByKeyword(String keyword) {
        return productRepository.getAllProductsByNameContainsOrderByNameAsc(keyword);
    }

    @Override
    public List<Product> getProductsByCartItems(List<CartItem> cartItems) {
        List<Product> products = new ArrayList<>();
        for(CartItem cartItem : cartItems) {
            Product product = productRepository.getProductById(cartItem.getId());
            products.add(product);
        }

        return products;
    }

    @Override
    public Product saveProduct(String productJsonString, MultipartFile file) throws Exception {

        String imageUrl;

        // convert json string to Product object
        Product product = mapper.readValue(productJsonString, Product.class);

        if(product == null || product.getId() != 0)
            return null;

        // put image in folder
        imageUrl = imageTool.putImageInFolder(file, product);

        // save the product to the database
        product.setCategory(productCategoryRepository.getProductCategoryById(product.getCategory().getId()));
        product.setImageUrl(imageUrl);
        product = productRepository.save(product);

        // delete image if product not saved
        if(product.getId() == 0)
            imageTool.deleteImage(imageUrl);

        return product;
    }

    @Override
    public Product editProduct(String productJsonString, MultipartFile file) throws Exception {

        String imageUrl;
        Product product;
        Product editedProduct;

        // convert json string to Product object
        product = mapper.readValue(productJsonString, Product.class);

        if(product == null)
            return null;

        // delete image
        imageTool.deleteImage(product.getId());

        // save new image
        imageUrl = imageTool.putImageInFolder(file, product);

        // edit the product in the database
        product.setCategory(productCategoryRepository.getProductCategoryById(product.getCategory().getId()));
        product.setImageUrl(imageUrl);
        editedProduct = productRepository.save(product);

        // delete image if product not saved
        if(product.equals(editedProduct))
            imageTool.deleteImage(imageUrl);

        return editedProduct;
    }

    @Override
    public void deleteProduct(int id) throws Exception {
        imageTool.deleteImage(id);
        productRepository.deleteById(id);
    }
}