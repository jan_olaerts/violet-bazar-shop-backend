package be.janolaerts.violetbazarshop.services;

import be.janolaerts.violetbazarshop.entity.Order;
import com.mercadopago.resources.Payment;
import com.sun.mail.smtp.SMTPTransport;
import org.springframework.stereotype.Service;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.transaction.Transactional;
import java.util.Date;
import java.util.Properties;

@Service
@Transactional
public class EmailService implements EmailService_I {

    @Override
    public void sendNewPasswordEmail(String firstName, String password, String email) throws MessagingException {
        String text = "Hola " + firstName + ", \n \n Su nueva contraseña es: " + password + "\n \n El equipo Violet Bazar";
        Message message = createMessage(text, email, EMAIL_SUBJECT_NEW_PASSWORD);
        sendEmail(message);
    }

    @Override
    public void sendOrderAcceptedEmail(Order order) throws MessagingException {
        StringBuilder sb = new StringBuilder();
        order.getCartItems().forEach(ci -> sb.append(ci.getName()).append(": ").append(ci.getQuantity()).append("\n"));
        String text = "Hola " + order.getUser().getFirstNames().split(" ")[0] + ", \n \n" +
                "El pago y su orden fueron aceptados! \n \n" +
                "Resumen de su orden: \n" +
                sb.toString() + "\n" +
                "Información de envío: \n" +
                order.getSend_firstNames() + " " + order.getSend_lastNames() + "\n" +
                order.getSend_address().getStreet() + " " + order.getSend_address().getNumber() + "\n" +
                order.getSend_address().getCity() + "\n" +
                order.getSend_address().getProvince() + "\n" +
                order.getSend_address().getDepartment() + "\n \n" +
                "El equipo Violet Bazar";
        Message message = createMessage(text, order.getUser().getEmail(), EMAIL_SUBJECT_ORDER_ACCEPTED);
        sendEmail(message);
    }

    @Override
    public void sendPaymentOkButNotEnoughStockEmail(Order order, String paymentId) throws MessagingException {
        String text = "Hola " + order.getUser().getFirstNames().split(" ")[0] + ", \n \n" +
                "El pago de la compra fue exitoso, sin embargo no hay suficiente stock de los productos que has pedido. \n \n" +
                "Por favor responder indicando los productos que compró con su cantidad y el codigo del pago: " + paymentId;
        Message message = createMessage(text, order.getUser().getEmail(), EMAIL_SUBJECT_PAYMENT_OK_NOT_ENOUGH_STOCK);
        sendEmail(message);
    }

    @Override
    public void sendCancelPaymentEmail(String firstName, String email, String paymentId) throws MessagingException {
        String text = "Hola " + firstName + ", \n \n" +
                "Hemos cancelado tu pago, si quieres comprar los productos, inténtalo de nuevo \n\n" +
                "El equipo Violet Bazar";
        Message message = createMessage(text, email, EMAIL_SUBJECT_CANCEL_PAYMENT);
        sendEmail(message);
    }

    @Override
    public void sendNotAbleToCancelPaymentEmail(String firstName, String email, String paymentId) throws MessagingException {
        String text = "Hola " + firstName + ",\n\n" +
                "Hemos intentado cancelar tu pago por estar en estado pendiente, pero sin resultado \n" +
                "Por favor constesta a este correo indicando los productos que ha comprado juntos con su cantidad y el siguiente código de pago: " + paymentId + "\n\n" +
                "El equipo Violet Bazar";
        Message message = createMessage(text, email, EMAIL_SUBJECT_NOT_ABLE_TO_CANCEL_PENDING_PAYMENT);
        sendEmail(message);
    }

    private Message createMessage(String text, String email, String subject) throws MessagingException {
        Message message = new MimeMessage(getEmailSession());
        message.setFrom(new InternetAddress(FROM_EMAIL));
        message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(email, false));
        message.setSubject(subject);
        message.setText(text);
        message.setSentDate(new Date());
        message.saveChanges();
        return message;
    }

    private Session getEmailSession() {
        Properties properties = System.getProperties();
        properties.put(SMTP_HOST, GMAIL_SMTP_SERVER);
        properties.put(SMTP_AUTH, true);
        properties.put(SMTP_PORT, DEFAULT_PORT);
        properties.put(SMTP_STARTTLS_ENABLE, true);
        properties.put(SMTP_STARTTLS_REQUIRED, true);
        return Session.getInstance(properties, null);
    }

    private void sendEmail(Message message) throws MessagingException {
        SMTPTransport smtpTransport = (SMTPTransport) getEmailSession().getTransport(SIMPLE_MAIL_TRANSFER_PROTOCOL);
        smtpTransport.connect(GMAIL_SMTP_SERVER, USERNAME, PASSWORD);
        smtpTransport.sendMessage(message, message.getAllRecipients());
        smtpTransport.close();
    }
}