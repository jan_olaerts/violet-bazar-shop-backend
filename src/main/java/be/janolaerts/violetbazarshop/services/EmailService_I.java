package be.janolaerts.violetbazarshop.services;

import be.janolaerts.violetbazarshop.entity.Order;
import com.mercadopago.resources.Payment;

import javax.mail.MessagingException;

public interface EmailService_I {

    String SIMPLE_MAIL_TRANSFER_PROTOCOL = "smtps";
    String GMAIL_SMTP_SERVER = "smtp.gmail.com";
    String USERNAME = "violetbazarperu@gmail.com";
    String PASSWORD = "Emprendimiento2020";
    String FROM_EMAIL = "violetbazarperu@gmail.com";
    String EMAIL_SUBJECT_NEW_PASSWORD = "Violet Bazar - Su nueva contraseña";
    String EMAIL_SUBJECT_ORDER_ACCEPTED = "Violet Bazar - Resumen de la compra";
    String EMAIL_SUBJECT_PAYMENT_OK_NOT_ENOUGH_STOCK = "Violet Bazar - Pago ok no stock sufuciente";
    String EMAIL_SUBJECT_CANCEL_PAYMENT = "Violet Bazar - Pago cancelado";
    String EMAIL_SUBJECT_NOT_ABLE_TO_CANCEL_PENDING_PAYMENT = "Violet Bazar - No posible cancelar pago pendiente";
    String SMTP_HOST = "mail.smtp.host";
    String SMTP_AUTH = "mail.smtp.auth";
    String SMTP_PORT = "mail.smtp.port";
    String SMTP_STARTTLS_ENABLE = "mail.smtp.starttls.enable";
    String SMTP_STARTTLS_REQUIRED = "mail.smtp.starttls.required";
    int DEFAULT_PORT = 465;

    void sendNewPasswordEmail(String firstName, String password, String email) throws MessagingException;
    void sendOrderAcceptedEmail(Order order) throws MessagingException;
    void sendPaymentOkButNotEnoughStockEmail(Order order, String paymentId) throws MessagingException;
    void sendCancelPaymentEmail(String firstName, String email, String paymentId) throws MessagingException;
    void sendNotAbleToCancelPaymentEmail(String firstName, String email, String paymentId) throws MessagingException;
}