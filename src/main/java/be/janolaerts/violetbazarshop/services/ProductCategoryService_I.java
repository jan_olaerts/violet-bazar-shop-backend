package be.janolaerts.violetbazarshop.services;

import be.janolaerts.violetbazarshop.entity.ProductCategory;

import java.util.List;

public interface ProductCategoryService_I {

    ProductCategory getProductCategoryById(int id);
    List<ProductCategory> getAllProductCategories();
    ProductCategory saveProductCategory(ProductCategory productCategory);
}