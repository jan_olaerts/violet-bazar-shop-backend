package be.janolaerts.violetbazarshop.services;

import be.janolaerts.violetbazarshop.entity.Address;
import be.janolaerts.violetbazarshop.entity.CartItem;
import be.janolaerts.violetbazarshop.entity.Order;
import be.janolaerts.violetbazarshop.entity.Product;
import be.janolaerts.violetbazarshop.exception.domain.NotEnoughStockException;
import be.janolaerts.violetbazarshop.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class OrderService implements OrderService_I {

    OrderRepository orderRepository;
    ProductRepository productRepository;
    UserRepository userRepository;
    AddressRepository addressRepository;
    CartItemRepository cartItemRepository;

    @Autowired
    public OrderService(OrderRepository orderRepository, ProductRepository productRepository, UserRepository userRepository, AddressRepository addressRepository, CartItemRepository cartItemRepository) {
        this.orderRepository = orderRepository;
        this.productRepository = productRepository;
        this.userRepository = userRepository;
        this.addressRepository = addressRepository;
        this.cartItemRepository = cartItemRepository;
    }

    @Override
    public Order getOrderById(int id) {
        return orderRepository.getOrderById(id);
    }

    @Override
    public List<Order> getAllOrdersByUserEmail(String email) {
        return orderRepository.getOrdersByUser_EmailOrderByPlacementTimeDesc(email);
    }

    @Override
    public List<Order> getAllOrders() {
        return orderRepository.getAllOrdersByOrderByPlacementTimeDesc();
    }

    @Override
    public Order saveOrder(Order order) throws NotEnoughStockException {

        if(order == null || order.getId() != 0)
            return null;

        // check stock
        for(CartItem cartItem : order.getCartItems()) {
            Product product = productRepository.getProductById(cartItem.getId());
            if(cartItem.getQuantity() > product.getStock())
                throw new NotEnoughStockException(NOT_ENOUGH_STOCK);
        }

        // subtract stock
        for(CartItem cartItem : order.getCartItems()) {
            Product product = productRepository.getProductById(cartItem.getId());
            product.setStock(product.getStock() - cartItem.getQuantity());
            productRepository.save(product);
        }

        // save address
        Address sendAddress = order.getSend_address();

        if(sendAddress.equals(order.getUser().getAddress())) {
            sendAddress.setId(order.getUser().getAddress().getId());
        }

        if(!sendAddress.equals(order.getUser().getAddress())) {
            List<Address> addresses = addressRepository.findAll();
            for(Address address : addresses) {
                if(address.equals(sendAddress)) {
                    sendAddress.setId(address.getId());
                    break;
                }
            }
        }

        addressRepository.save(sendAddress);
        return orderRepository.save(order);
    }
}