package be.janolaerts.violetbazarshop.services;

import be.janolaerts.violetbazarshop.entity.CartItem;
import be.janolaerts.violetbazarshop.entity.Product;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface ProductService_I {

    Product getProductById(int id);
    List<Product> getAllProducts();
    List<Product> getAllProductsByProductCategoryId(int productCategoryId);
    List<Product> getAllProductsByKeyword(String keyword);
    List<Product> getProductsByCartItems(List<CartItem> cartItems);
    Product saveProduct(String productJsonString, MultipartFile file) throws Exception;
    Product editProduct(String productJsonString, MultipartFile file) throws Exception;
    void deleteProduct(int id) throws Exception;
}