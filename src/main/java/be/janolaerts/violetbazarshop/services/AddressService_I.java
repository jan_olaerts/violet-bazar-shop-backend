package be.janolaerts.violetbazarshop.services;

import be.janolaerts.violetbazarshop.entity.Address;
import be.janolaerts.violetbazarshop.entity.Product;

import java.util.List;

public interface AddressService_I {

    Address getAddressById(int id);
    List<Address> getAllAddresses();
    Address saveAddress(Address address) throws Exception;
    void deleteAddress(int id) throws Exception;
}