package be.janolaerts.violetbazarshop.services;

import be.janolaerts.violetbazarshop.entity.CartItem;
import be.janolaerts.violetbazarshop.entity.User;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mercadopago.MercadoPago;
import com.mercadopago.exceptions.MPException;
import com.mercadopago.resources.Payment;
import com.mercadopago.resources.Preference;
import com.mercadopago.resources.datastructures.customer.Shipment;
import com.mercadopago.resources.datastructures.preference.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import java.util.ArrayList;
import java.util.List;

@Service
public class PaymentService implements PaymentService_I {

    private ObjectMapper mapper;
    private EmailService_I emailService;

    @Autowired
    public PaymentService(EmailService_I emailService) {
        this.mapper = new ObjectMapper();
        this.emailService = emailService;
    }

    @Override
    public String pay(User user, List<CartItem> cartItems) throws MPException {

        MercadoPago.SDK.setAccessToken(MP_TOKEN);

        Preference preference = new Preference();
        setBackUrlsForPreference(preference);
        addItems(preference, cartItems);

        PaymentMethods paymentMethods = new PaymentMethods();
        paymentMethods.setExcludedPaymentMethods("pagoefectivo_atm");
        preference.setPaymentMethods(paymentMethods);

        Payer payer = new Payer();
        payer.setName(user.getFirstNames());
        payer.setSurname(user.getLastName_1() + " " + user.getLastName_2());
        payer.setEmail(user.getEmail());
        payer.setAuthenticationType("Web Nativa");

        Address address = new Address();
        address.setStreetName(user.getAddress().getStreet());

        Identification identification = new Identification();
        identification.setType("dni");
        identification.setNumber(user.getDni());
        payer.setIdentification(identification);

        preference.setPayer(payer);

        return preference.save().getInitPoint();
    }

    public String cancelPayment(String firstName, String email, String paymentId) throws MessagingException {

        String message;
        try {
            Payment payment = Payment.findById(paymentId);
            payment.setStatus(Payment.Status.cancelled);
            payment.update();
            message = CANCEL_SUCCESS;
            emailService.sendCancelPaymentEmail(firstName, email, paymentId);
        } catch (Exception ex) {
            message = CANCEL_FAIL;
            emailService.sendNotAbleToCancelPaymentEmail(firstName, email, paymentId);
        }

        return message;
    }

    private void setBackUrlsForPreference(Preference preference) {
        preference.setBackUrls(
                new BackUrls()
                        .setFailure(URL_FAILURE)
                        .setPending(URL_PENDING)
                        .setSuccess(URL_SUCCESS)
        );
    }

    private void addItems(Preference preference, List<CartItem> cartItems) {
        List<Item> items = new ArrayList<>();
        for(CartItem cartItem : cartItems) {
            Item item = new Item();
            item
                    .setId(String.valueOf(cartItem.getId()))
                    .setTitle(cartItem.getName())
                    .setQuantity(cartItem.getQuantity())
                    .setUnitPrice(Float.valueOf(String.valueOf(cartItem.getPrice())))
                    .setCurrencyId("PEN");
            items.add(item);
        }

        preference.setItems((ArrayList<Item>) items);
    }
}