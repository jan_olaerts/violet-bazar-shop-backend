package be.janolaerts.violetbazarshop.services;

public interface LoginAttemptService_I {

    int MAXIMUM_NUMBER_OF_ATTEMPTS = 5;
    int ATTEMPT_INCREMENT = 1;

    void addUserToLoginAttemptCache(String email);
    void evictUserFromLoginAttemptCache(String email);
    boolean hasExceededMaxAttempts(String email);
}