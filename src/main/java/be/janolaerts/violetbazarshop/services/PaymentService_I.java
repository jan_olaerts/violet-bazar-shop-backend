package be.janolaerts.violetbazarshop.services;

import be.janolaerts.violetbazarshop.entity.CartItem;
import be.janolaerts.violetbazarshop.entity.User;
import com.mercadopago.exceptions.MPException;

import javax.mail.MessagingException;
import java.io.IOException;
import java.util.List;

public interface PaymentService_I {

    String MP_TOKEN = "APP_USR-3290359279060632-010222-de05d60895d813831f018ede0067a9a8-670671549";
    String URL_FAILURE = "http://localhost:4200/shoppingcart?payment=fail";
    String URL_PENDING = "http://localhost:4200/shoppingcart?payment=pending";
    String URL_SUCCESS = "http://localhost:4200/shoppingcart?payment=success";
    String CANCEL_SUCCESS = "Hemos cancelado su pago, inténtalo  de nuevo";
    String CANCEL_FAIL = "No pudimos cancelar su pago, envíanos  un email";

    String pay(User user, List<CartItem> cartItems) throws MPException, IOException;
    String cancelPayment(String firstName, String email, String paymentId) throws MPException, MessagingException;
}