package be.janolaerts.violetbazarshop.services;

import be.janolaerts.violetbazarshop.entity.User;
import be.janolaerts.violetbazarshop.exception.domain.DniAlreadyExistsException;
import be.janolaerts.violetbazarshop.exception.domain.EmailAlreadyExistsException;
import be.janolaerts.violetbazarshop.exception.domain.EmailNotFoundException;
import be.janolaerts.violetbazarshop.exception.domain.UserNotFoundException;
import org.springframework.security.core.userdetails.UserDetails;

import javax.mail.MessagingException;
import java.util.List;

public interface UserService_I {

    String NO_USER_FOUND_BY_EMAIL = "No encontramos usuario con email: ";
    String FOUND_USER_BY_EMAIL = "Returning found user by email: ";
    String EMAIL_ALREADY_EXISTS = "Email ya existe";
    String DNI_ALREADY_EXISTS = "Dni ya existe";

    UserDetails loadUserByEmail(String email) throws EmailNotFoundException;
    User register(User user) throws UserNotFoundException, EmailAlreadyExistsException, MessagingException, DniAlreadyExistsException;
    List<User> getUsers();
    User findUserByEmail(String email);
    User updateUser(User user);
    void deleteUser(String email);
    void resetPassword(String email, String password) throws EmailNotFoundException, MessagingException;
}