package be.janolaerts.violetbazarshop.services;

import be.janolaerts.violetbazarshop.entity.Address;
import be.janolaerts.violetbazarshop.repository.AddressRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class AddressService implements AddressService_I {

    AddressRepository addressRepository;

    @Autowired
    public AddressService(AddressRepository addressRepository) {
        this.addressRepository = addressRepository;
    }

    @Override
    public Address getAddressById(int id) {
        return addressRepository.getAddressById(id);
    }

    @Override
    public List<Address> getAllAddresses() {
        return addressRepository.findAll();
    }

    @Override
    public Address saveAddress(Address address) throws Exception {

        if(address == null || address.getId() != 0)
            return null;

        return addressRepository.save(address);
    }

    @Override
    public void deleteAddress(int id) throws Exception {
        addressRepository.deleteById(id);
    }
}