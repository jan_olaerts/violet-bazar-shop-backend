package be.janolaerts.violetbazarshop.services;

import be.janolaerts.violetbazarshop.entity.CartItem;
import be.janolaerts.violetbazarshop.entity.Order;

import java.util.List;

public interface CartItemService_I {

    CartItem getCartItemById(int id);
    List<CartItem> getAllCartItems();
    List<CartItem> getAllCartItemsByOrderId(int orderId);
    CartItem saveCartItem(CartItem cartItem);
    void saveCartItemsByOrder(Order order);
    void deleteCartItem(int id);
}