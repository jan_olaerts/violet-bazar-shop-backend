package be.janolaerts.violetbazarshop.services;

import be.janolaerts.violetbazarshop.entity.Order;
import be.janolaerts.violetbazarshop.exception.domain.NotEnoughStockException;

import java.util.List;

public interface OrderService_I {

    String NOT_ENOUGH_STOCK = "No hay suficiente stock";

    Order getOrderById(int id);
    List<Order> getAllOrdersByUserEmail(String email);
    List<Order> getAllOrders();
    Order saveOrder(Order order) throws NotEnoughStockException;
}