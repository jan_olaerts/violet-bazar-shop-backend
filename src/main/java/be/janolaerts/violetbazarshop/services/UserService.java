package be.janolaerts.violetbazarshop.services;

import be.janolaerts.violetbazarshop.domain.UserPrincipal;
import be.janolaerts.violetbazarshop.entity.Address;
import be.janolaerts.violetbazarshop.entity.User;
import be.janolaerts.violetbazarshop.enumeration.Role;
import be.janolaerts.violetbazarshop.exception.domain.DniAlreadyExistsException;
import be.janolaerts.violetbazarshop.exception.domain.EmailAlreadyExistsException;
import be.janolaerts.violetbazarshop.exception.domain.EmailNotFoundException;
import be.janolaerts.violetbazarshop.repository.AddressRepository;
import be.janolaerts.violetbazarshop.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.transaction.Transactional;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Service
@Transactional
@Qualifier("UserDetailsService")
public class UserService implements UserService_I, UserDetailsService {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserService.class);
    private UserRepository userRepository;
    private AddressRepository addressRepository;
    private BCryptPasswordEncoder bCryptPasswordEncoder;
    private LoginAttemptService loginAttemptService;
    private EmailService mailService;

    @Autowired
    public UserService(UserRepository userRepository, AddressRepository addressRepository, BCryptPasswordEncoder bCryptPasswordEncoder,
                       LoginAttemptService loginAttemptService, EmailService mailService) {

        this.userRepository = userRepository;
        this.addressRepository = addressRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.loginAttemptService = loginAttemptService;
        this.mailService = mailService;
    }

    @Override
    public UserDetails loadUserByEmail(String email) throws EmailNotFoundException {
        User user = userRepository.findUserByEmail(email);
        if(user == null) {
            LOGGER.error(NO_USER_FOUND_BY_EMAIL + email);
            throw new EmailNotFoundException(NO_USER_FOUND_BY_EMAIL + email);
        } else {
            validateLoginAttempt(user);
            user.setLastLoginDateDisplay(user.getLastLoginDate());
            user.setLastLoginDate(new Date());
            userRepository.save(user);
            UserPrincipal userPrincipal = new UserPrincipal(user);
            LOGGER.info(FOUND_USER_BY_EMAIL + email);
            return userPrincipal;
        }
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        try {
            return loadUserByEmail(email);
        } catch (EmailNotFoundException ex) {
            ex.printStackTrace();
        }

        return null;
    }

    @Override
    public User register(User user) throws EmailAlreadyExistsException, MessagingException, DniAlreadyExistsException {
        validateNewDni(user.getDni());
        validateNewEmail(user.getEmail());
        User newUser = new User();
        newUser.setDni(user.getDni());
        newUser.setFirstNames(user.getFirstNames());
        newUser.setLastName_1(user.getLastName_1());
        newUser.setLastName_2(user.getLastName_2());
        newUser.setEmail(user.getEmail());
        newUser.setJoinDate(new Date());
        newUser.setPassword(encodePassword(user.getPassword()));
        newUser.setActive(true);
        newUser.setNotLocked(true);
        newUser.setRole(Role.ROLE_USER.name());
        newUser.setAuthorities(Role.ROLE_USER.getAuthorities());
        newUser.setAddress(user.getAddress());
        addressRepository.save(user.getAddress());
        userRepository.save(newUser);
        LOGGER.info("User password: " + newUser.getPassword());
        mailService.sendNewPasswordEmail(user.getFirstNames(), user.getPassword(), user.getEmail());
        return newUser;
    }

    @Override
    public List<User> getUsers() {
        return userRepository.findAll();
    }

    @Override
    public User findUserByEmail(String email) {
        return userRepository.findUserByEmail(email);
    }

    @Override
    public User updateUser(User user) {

        User userBeforeEdit = findUserByEmail(user.getEmail());

        // save address
        Address newAddress = user.getAddress();

        if(newAddress.equals(userBeforeEdit.getAddress())) {
            newAddress.setId(userBeforeEdit.getAddress().getId());
        }

        if(!newAddress.equals(userBeforeEdit.getAddress())) {
            List<Address> addresses = addressRepository.findAll();
            for(Address address : addresses) {
                if(address.equals(newAddress)) {
                    newAddress.setId(address.getId());
                    break;
                }
            }
        }

        addressRepository.save(user.getAddress());

        user.setActive(userBeforeEdit.isActive());
        user.setNotLocked(userBeforeEdit.isNotLocked());
        user.setJoinDate(userBeforeEdit.getJoinDate());
        user.setLastLoginDate(userBeforeEdit.getLastLoginDate());
        user.setLastLoginDateDisplay(userBeforeEdit.getLastLoginDateDisplay());
        user.setPassword(userBeforeEdit.getPassword());
        user.setRole(userBeforeEdit.getRole());

        if(user.getRole().equals("ROLE_ADMIN"))
            user.setAuthorities(Role.ROLE_ADMIN.getAuthorities());

        else
            user.setAuthorities(Role.ROLE_USER.getAuthorities());

        User updatedUser = userRepository.save(user);
        return updatedUser;
    }

    @Override
    public void deleteUser(String email) {
        User user = findUserByEmail(email);
        if(user != null) userRepository.delete(user);
    }

    @Override
    public void resetPassword(String email, String password) throws EmailNotFoundException, MessagingException {
        User user = findUserByEmail(email);
        if(user == null) throw new EmailNotFoundException(NO_USER_FOUND_BY_EMAIL + email);

        user.setPassword(encodePassword(password));
        userRepository.save(user);
        LOGGER.info("New user password: " + password);
        mailService.sendNewPasswordEmail(user.getFirstNames(), password, user.getEmail());
    }

    private void validateLoginAttempt(User user) {

        if(user.isNotLocked()) {

            if(loginAttemptService.hasExceededMaxAttempts(user.getEmail()))
                user.setNotLocked(false);

            else
                user.setNotLocked(true);

        } else {
            loginAttemptService.evictUserFromLoginAttemptCache(user.getEmail());
        }
    }

    private void validateNewEmail(String newEmail) throws EmailAlreadyExistsException {

        User userByNewEmail = findUserByEmail(newEmail);

        if(userByNewEmail != null) throw new EmailAlreadyExistsException(EMAIL_ALREADY_EXISTS);
    }

    private void validateNewDni(String dni) throws DniAlreadyExistsException {

        User userByNewDni = userRepository.findUserByDni(dni);
        if(userByNewDni != null) throw new DniAlreadyExistsException(DNI_ALREADY_EXISTS);
    }

    private String encodePassword(String password) {
        return bCryptPasswordEncoder.encode(password);
    }
}