package be.janolaerts.violetbazarshop.services;


import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import org.springframework.stereotype.Service;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

@Service
public class LoginAttemptService implements LoginAttemptService_I {

    private LoadingCache<String, Integer> loginAttemptCache;

    public LoginAttemptService() {
        this.loginAttemptCache = CacheBuilder.newBuilder().expireAfterWrite(15, TimeUnit.MINUTES)
                .maximumSize(100).build(new CacheLoader<>() {

                    public Integer load(String key) {
                        return 0;
                    }
                });
    }

    @Override
    public void addUserToLoginAttemptCache(String email) {
        int attempts = 0;
        try {
            attempts = ATTEMPT_INCREMENT + loginAttemptCache.get(email);
        } catch (ExecutionException ex) {
            ex.printStackTrace();
        }
        loginAttemptCache.put(email, attempts);
    }

    @Override
    public void evictUserFromLoginAttemptCache(String email) {
        loginAttemptCache.invalidate(email);
    }

    @Override
    public boolean hasExceededMaxAttempts(String email) {
        try {
            return loginAttemptCache.get(email) >= MAXIMUM_NUMBER_OF_ATTEMPTS;
        } catch (ExecutionException ex) {
            ex.printStackTrace();
        }

        return false;
    }
}