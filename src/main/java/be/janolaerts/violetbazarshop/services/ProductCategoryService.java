package be.janolaerts.violetbazarshop.services;

import be.janolaerts.violetbazarshop.entity.ProductCategory;
import be.janolaerts.violetbazarshop.repository.ProductCategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class ProductCategoryService implements ProductCategoryService_I {

    private ProductCategoryRepository productCategoryRepository;

    @Autowired
    public ProductCategoryService(ProductCategoryRepository productCategoryRepository) {
        this.productCategoryRepository = productCategoryRepository;
    }

    @Override
    public ProductCategory getProductCategoryById(int id) {
        return productCategoryRepository.getProductCategoryById(id);
    }

    @Override
    public List<ProductCategory> getAllProductCategories() {
        return productCategoryRepository.getAllByOrderByCategoryName();
    }

    @Override
    public ProductCategory saveProductCategory(ProductCategory productCategory) {

        if(productCategory == null || productCategory.getId() != 0)
            return null;

        return productCategoryRepository.save(productCategory);
    }
}