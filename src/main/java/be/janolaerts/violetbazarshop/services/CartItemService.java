package be.janolaerts.violetbazarshop.services;

import be.janolaerts.violetbazarshop.entity.CartItem;
import be.janolaerts.violetbazarshop.entity.Order;
import be.janolaerts.violetbazarshop.repository.CartItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class CartItemService implements CartItemService_I {

    private CartItemRepository cartItemRepository;

    @Autowired
    public CartItemService(CartItemRepository cartItemRepository) {
        this.cartItemRepository = cartItemRepository;
    }

    @Override
    public CartItem getCartItemById(int id) {
        return cartItemRepository.getCartItemById(id);
    }

    @Override
    public List<CartItem> getAllCartItems() {
        return cartItemRepository.findAll();
    }

    @Override
    public List<CartItem> getAllCartItemsByOrderId(int orderId) {
        return cartItemRepository.getCartItemsByOrder_IdOrderByName(orderId);
    }

    @Override
    public CartItem saveCartItem(CartItem cartItem) {
        if(cartItem == null) return null;

        return cartItemRepository.save(cartItem);
    }

    @Override
    public void saveCartItemsByOrder(Order order) {
        for(CartItem cartItem : order.getCartItems()) {
            cartItem.setId(0);
            cartItem.setOrder(order);
            saveCartItem(cartItem);
        }
    }

    @Override
    public void deleteCartItem(int id) {
        cartItemRepository.deleteById(id);
    }
}