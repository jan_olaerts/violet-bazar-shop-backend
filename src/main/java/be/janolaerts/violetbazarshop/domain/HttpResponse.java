package be.janolaerts.violetbazarshop.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.http.HttpStatus;

import java.util.Date;

public class HttpResponse {

    private int httpStatusCode;
    private HttpStatus httpStatus;
    private String reason;
    private String message;

    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="MM-dd-yyyy hh:mm:ss", timezone="Europe/Brussels")
    private Date timeStamp;

    public HttpResponse() {
    }

    public HttpResponse(int httpStatusCode, HttpStatus httpStatus, String reason, String message) {
        this.httpStatusCode = httpStatusCode;
        this.httpStatus = httpStatus;
        this.reason = reason;
        this.message = message;
        this.timeStamp = new Date();
    }

    public int getHttpStatusCode() {
        return httpStatusCode;
    }

    public void setHttpStatusCode(int httpStatusCode) {
        if(httpStatusCode < 100) throw new IllegalArgumentException("httpStatusCode cannot be smaller than 100");
        if(httpStatusCode >= 600) throw new IllegalArgumentException("httpStatusCode cannot be higher or equal to 600");

        this.httpStatusCode = httpStatusCode;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    public void setHttpStatus(HttpStatus httpStatus) {
        if(httpStatus == null) throw new IllegalArgumentException("httpStatus cannot be null");

        this.httpStatus = httpStatus;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        if(reason == null) throw new IllegalArgumentException("reason cannot be null");
        if(reason.length() < 2) throw new IllegalArgumentException("reason must have at least 2 characters");

        this.reason = reason;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        if(message == null) throw new IllegalArgumentException("message cannot be null");
        if(message.length() < 2) throw new IllegalArgumentException("message must have at least 2 characters");

        this.message = message;
    }

    public Date getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(Date timeStamp) {
        if(timeStamp == null) throw new IllegalArgumentException("timeStamp cannot be null");
        if(timeStamp.after(new Date())) throw new IllegalArgumentException("timeStamp cannot be in the future");

        this.timeStamp = timeStamp;
    }
}
