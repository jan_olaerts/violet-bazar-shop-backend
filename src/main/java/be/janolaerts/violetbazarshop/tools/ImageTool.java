package be.janolaerts.violetbazarshop.tools;

import be.janolaerts.violetbazarshop.entity.Product;
import be.janolaerts.violetbazarshop.repository.ProductRepository;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

@Component
public class ImageTool {

    @Value("${products_location}")
    private String productsLocation;

    @Value("${cart_items_location}")
    private String cartItemsLocation;

    private ProductRepository productRepository;

    @Autowired
    public ImageTool(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    public Map<String, byte[]> getAllImagesFromFolder() throws IOException {

        File dir = new File(productsLocation);
        String imageName;
        byte[] imageBytes;
        HashMap<String, byte[]> images = new HashMap<>();

        if(dir.isDirectory()) {

            for(File file : Objects.requireNonNull(dir.listFiles())) {

                imageName = file.getName();
                imageBytes = FileUtils.readFileToByteArray(file);
                images.put(imageName, imageBytes);
            }
        }

        return images;
    }

    public String putImageInFolder(MultipartFile file, Product product) throws Exception {

        // put the image in the images folder
        int nameLength = Objects.requireNonNull(file.getOriginalFilename()).length();
        int beginPosition = file.getOriginalFilename().contains(".jpeg") ? nameLength -5 : nameLength -4;
        String extension = file.getOriginalFilename().substring(beginPosition);

        String imageUrl = product.getCategory().getCategoryName().toLowerCase() + "_" +
                product.getBrand().toLowerCase() + "_" + product.getName().toLowerCase() + extension;

        file.transferTo(new File(productsLocation + "\\" + imageUrl));

        try {
            // save image in cart_images folder
            Files.copy(new File(productsLocation + "\\" + imageUrl).toPath(), new File(cartItemsLocation + "\\" + imageUrl).toPath());
        } catch (FileAlreadyExistsException ex) {
            deleteImageFromCartItemsLocation(imageUrl);
            Files.copy(new File(productsLocation + "\\" + imageUrl).toPath(), new File(cartItemsLocation + "\\" + imageUrl).toPath());
        }

        return imageUrl;
    }

    public void deleteImage(int productId) throws Exception {

        Product product = productRepository.getProductById(productId);
        String imageName = product.getImageUrl();
        File dir = new File(productsLocation);

        if(dir.isDirectory()) {

            for(File file : Objects.requireNonNull(dir.listFiles())) {

                if(file.getName().equalsIgnoreCase(imageName))
                    Files.deleteIfExists(Paths.get(productsLocation + "\\" + imageName));
            }
        }
    }

    public void deleteImage(String imageUrl) throws Exception {

        File dir = new File(productsLocation);

        if(dir.isDirectory()) {

            for(File file : Objects.requireNonNull(dir.listFiles())) {

                if(file.getName().equals(imageUrl))
                    Files.deleteIfExists(Paths.get(productsLocation + "\\" + imageUrl));
            }
        }
    }

    public void deleteImageFromCartItemsLocation(String imageUrl) throws Exception {

        File dir = new File(cartItemsLocation);

        if(dir.isDirectory()) {

            for(File file : Objects.requireNonNull(dir.listFiles())) {

                if(file.getName().equals(imageUrl))
                    Files.deleteIfExists(Paths.get(cartItemsLocation + "\\" + imageUrl));
            }
        }
    }
}