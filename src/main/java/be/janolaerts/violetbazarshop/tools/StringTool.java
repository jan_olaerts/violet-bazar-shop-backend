package be.janolaerts.violetbazarshop.tools;

import java.util.regex.Pattern;

public class StringTool {

    public static boolean validateEmail(String email) {
        if(email == null) throw new IllegalArgumentException("email cannot be null");

        email = email.toLowerCase();
        return Pattern.compile("^([a-z\\d\\.-]+)@([a-z\\d-]+)\\.([a-z]{2,8})(\\.[a-z]{2,8})?$").matcher(email).find();
    }

    public static String capitalizeFirstLetters(String text) {
        if(text == null) throw new IllegalArgumentException("text cannot be null");

        StringBuilder sb = new StringBuilder();
        boolean capitalize = false;
        for(int i = 0; i < text.length(); i++) {
            char letter = text.charAt(i);
            letter = Character.toLowerCase(letter);
            if(i == 0 || capitalize) letter = Character.toUpperCase(letter);
            capitalize = false;

            sb.append(letter);
            if(letter == ' ' || letter == '-') capitalize = true;
        }

        return sb.toString();
    }
}