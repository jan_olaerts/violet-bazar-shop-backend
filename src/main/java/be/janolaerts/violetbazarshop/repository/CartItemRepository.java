package be.janolaerts.violetbazarshop.repository;

import be.janolaerts.violetbazarshop.entity.CartItem;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CartItemRepository extends JpaRepository<CartItem, Integer> {

    default CartItem getCartItemById(int id) {
        return getOne(id);
    }
    List<CartItem> getCartItemsByOrder_IdOrderByName(int id);
}