package be.janolaerts.violetbazarshop.repository;

import be.janolaerts.violetbazarshop.entity.Address;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AddressRepository extends JpaRepository<Address, Integer> {

    default Address getAddressById(int id) {
        return getOne(id);
    }
}