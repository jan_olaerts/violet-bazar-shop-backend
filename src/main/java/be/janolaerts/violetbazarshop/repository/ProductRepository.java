package be.janolaerts.violetbazarshop.repository;

import be.janolaerts.violetbazarshop.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProductRepository extends JpaRepository<Product, Integer> {

    default Product getProductById(int id) {
        return getOne(id);
    }
    List<Product> getAllProductsByOrderByCategoryAsc();
    List<Product> getAllProductsByCategory_IdOrderByName(int productCategoryId);
    List<Product> getAllProductsByNameContainsOrderByNameAsc(String keyword);
}