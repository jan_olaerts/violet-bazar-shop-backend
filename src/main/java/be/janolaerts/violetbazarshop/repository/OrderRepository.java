package be.janolaerts.violetbazarshop.repository;

import be.janolaerts.violetbazarshop.entity.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface OrderRepository extends JpaRepository<Order, Integer> {

    default Order getOrderById(int id) {
        return getOne(id);
    }
    List<Order> getOrdersByUser_EmailOrderByPlacementTimeDesc(String email);
    List<Order> getAllOrdersByOrderByPlacementTimeDesc();
}