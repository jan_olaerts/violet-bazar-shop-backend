package be.janolaerts.violetbazarshop.repository;

import be.janolaerts.violetbazarshop.entity.ProductCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

@RepositoryRestResource(collectionResourceRel="productCategory", path="product-category")
public interface ProductCategoryRepository extends JpaRepository<ProductCategory, Integer> {

    default ProductCategory getProductCategoryById(int id) {
        return getOne(id);
    }

    List<ProductCategory> getAllByOrderByCategoryName();
}