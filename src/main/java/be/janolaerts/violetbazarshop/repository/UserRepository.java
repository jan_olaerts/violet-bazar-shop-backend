package be.janolaerts.violetbazarshop.repository;

import be.janolaerts.violetbazarshop.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, String> {

    User findUserByEmail(String email);
    User findUserByDni(String dni);
    void deleteUserByEmail(String email);
}