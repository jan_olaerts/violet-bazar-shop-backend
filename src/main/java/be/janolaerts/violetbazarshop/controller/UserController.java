package be.janolaerts.violetbazarshop.controller;

import be.janolaerts.violetbazarshop.domain.HttpResponse;
import be.janolaerts.violetbazarshop.domain.UserPrincipal;
import be.janolaerts.violetbazarshop.entity.User;
import be.janolaerts.violetbazarshop.exception.ExceptionHandling;
import be.janolaerts.violetbazarshop.exception.domain.EmailNotFoundException;
import be.janolaerts.violetbazarshop.services.AddressService;
import be.janolaerts.violetbazarshop.services.UserService;
import be.janolaerts.violetbazarshop.utility.JWTTokenProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import javax.mail.MessagingException;
import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping("/users")
public class UserController extends ExceptionHandling {

    private UserService userService;
    private AddressService addressService;
    private AuthenticationManager authenticationManager;
    private JWTTokenProvider jwtTokenProvider;
    private final RestTemplate restTemplate;

    private static final String EMAIL_SENT = "An email with a new password was sent to: ";
    private static final String USER_DELETED_SUCCESSFULLY = "User deleted successfully";
    private static final String JWT_TOKEN_HEADER = "Jwt-Token";
    private static final String PERU_API_BASE_URL = "https://apiperu.dev/api/dni";
    private static final String PERU_API_TOKEN = "e9b4264bf8d893ee1f22328919afb4dd37501e36c5ebab8f85628464352958c2";

    @Autowired
    public UserController(UserService userService,
                          AddressService addressService,
                          AuthenticationManager authenticationManager,
                          JWTTokenProvider jwtTokenProvider,
                          RestTemplate restTemplate) {

        this.userService = userService;
        this.addressService = addressService;
        this.authenticationManager = authenticationManager;
        this.jwtTokenProvider = jwtTokenProvider;
        this.restTemplate = restTemplate;
    }

    @GetMapping(path="getPerson/{dni}", produces="application/json")
    public ResponseEntity<Object> getPerson(@PathVariable("dni") String dni) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + PERU_API_TOKEN);
        HttpEntity<Object> entity = new HttpEntity<>(headers);
        return restTemplate.exchange(PERU_API_BASE_URL + "/" + dni, HttpMethod.GET, entity, Object.class);
    }

    @PostMapping(path="/register", consumes="application/json", produces="application/json")
    public ResponseEntity<User> register(@RequestBody User user) throws Exception {
        User newUser = userService.register(user);
        return new ResponseEntity<>(newUser, HttpStatus.OK);
    }

    @PostMapping(path="/login", consumes= {MediaType.MULTIPART_FORM_DATA_VALUE}, produces="application/json")
    public ResponseEntity<User> login(@RequestParam("email") String email, @RequestParam("password") String password) {
        authenticate(email, password);
        User loginUser = userService.findUserByEmail(email);
        UserPrincipal userPrincipal = new UserPrincipal(loginUser);
        HttpHeaders jwtHeader = getJwtHeader(userPrincipal);
        return new ResponseEntity<>(loginUser, jwtHeader, HttpStatus.OK);
    }

    @GetMapping(path="/find/{email}", produces="application/json")
    @PreAuthorize("hasAuthority('ROLE_ADMIN') or #email == principal")
    public ResponseEntity<User> getUser(@PathVariable("email") String email) {
        User user = userService.findUserByEmail(email);
        return new ResponseEntity<>(user, HttpStatus.OK);
    }

    @GetMapping(path="/getAll", produces="application/json")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public ResponseEntity<List<User>> getAllUsers() {
        List<User> users = userService.getUsers();
        return new ResponseEntity<>(users, HttpStatus.OK);
    }

    @PostMapping(path="/resetPassword", consumes={MediaType.MULTIPART_FORM_DATA_VALUE}, produces="application/json")
    @PreAuthorize("#email == principal")
    public ResponseEntity<HttpResponse> resetPassword(@RequestParam("email") String email, @RequestParam("password") String newPassword) throws EmailNotFoundException, MessagingException {
        userService.resetPassword(email, newPassword);
        return response(HttpStatus.OK, EMAIL_SENT + email);
    }

    @PatchMapping(path="/update", consumes="application/json", produces="application/json")
    @PreAuthorize("#user.email == principal")
    public ResponseEntity<User> updateUser(@RequestBody User user) {
        user = userService.updateUser(user);
        return user != null ? new ResponseEntity<>(user, HttpStatus.OK) : ResponseEntity.badRequest().build();
    }

    @DeleteMapping(path="/{email}", consumes="application/json")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public ResponseEntity<HttpResponse> deleteUser(@PathVariable("email") String email) {
        userService.deleteUser(email);
        return ResponseEntity.ok().build();
    }

    private void authenticate(String email, String password) {
        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(email, password));
    }

    private HttpHeaders getJwtHeader(UserPrincipal userPrincipal) {
        HttpHeaders headers = new HttpHeaders();
        headers.add(JWT_TOKEN_HEADER, jwtTokenProvider.generateJwtToken(userPrincipal));
        return headers;
    }

    private ResponseEntity<HttpResponse> response(HttpStatus httpStatus, String message) {
        return new ResponseEntity<>(new HttpResponse(httpStatus.value(), httpStatus, httpStatus.getReasonPhrase(), message), httpStatus);
    }
}