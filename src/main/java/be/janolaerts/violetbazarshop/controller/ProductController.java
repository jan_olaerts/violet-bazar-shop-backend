package be.janolaerts.violetbazarshop.controller;

import be.janolaerts.violetbazarshop.entity.CartItem;
import be.janolaerts.violetbazarshop.entity.Product;
import be.janolaerts.violetbazarshop.exception.ExceptionHandling;
import be.janolaerts.violetbazarshop.services.ProductService_I;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
@RequestMapping("/products")
public class ProductController extends ExceptionHandling {

    private ProductService_I productService;

    @Autowired
    public ProductController(ProductService_I productService) {
        this.productService = productService;
    }

    @GetMapping(path="/getById/{id:^\\d+$}", produces="application/json")
    public ResponseEntity<Product> getByIdHandler(@PathVariable("id") int id) {
        Product product = productService.getProductById(id);
        return product != null ? ResponseEntity.ok(product) : ResponseEntity.badRequest().build();
    }

    @GetMapping(path="/getAll", produces="application/json")
    public ResponseEntity<List<Product>> getAllHandler() {
        List<Product> products = productService.getAllProducts();
        return ResponseEntity.ok(products);
    }

    @GetMapping(path="/byCategoryId/{categoryId:^\\d+$}", produces="application/json")
    public ResponseEntity<List<Product>> getByProductCategoryId(@PathVariable("categoryId") int productCategoryId) {
        List<Product> products = productService.getAllProductsByProductCategoryId(productCategoryId);
        return ResponseEntity.ok(products);
    }

    @GetMapping(path="/lookup", params={"keyword"}, produces="application/json")
    public ResponseEntity<List<Product>> getByKeyword(@RequestParam("keyword") String keyword) {
        List<Product> products = productService.getAllProductsByKeyword(keyword);
        return ResponseEntity.ok(products);
    }

    @PostMapping(path="/byCartItems", consumes="application/json", produces="application/json")
    public ResponseEntity<List<Product>> getByCartItems(@RequestBody List<CartItem> cartItems) {
        List<Product> products = productService.getProductsByCartItems(cartItems);
        return ResponseEntity.ok(products);
    }

    @PostMapping(path="/post", consumes={MediaType.MULTIPART_FORM_DATA_VALUE}, produces="application/json")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public ResponseEntity<Product> postHandler(@RequestParam("product") String productJsonString,
                                                @RequestParam("imageFile") MultipartFile file) throws Exception {

        Product product = productService.saveProduct(productJsonString, file);
        return product != null ? ResponseEntity.ok(product) : ResponseEntity.badRequest().build();
    }

    @PatchMapping(path="/edit", consumes={MediaType.MULTIPART_FORM_DATA_VALUE}, produces="application/json")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public ResponseEntity<Product> patchHandler(@RequestParam("product") String productJsonString,
                                                 @RequestParam("imageFile") MultipartFile file) throws Exception {

        Product product = productService.editProduct(productJsonString, file);
        return product != null ? ResponseEntity.ok(product) : ResponseEntity.badRequest().build();
    }

    @DeleteMapping(path="/delete/{id:^\\d+$}")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public ResponseEntity<?> deleteHandler(@PathVariable("id") int id) throws Exception {
        productService.deleteProduct(id);
        return ResponseEntity.ok().build();
    }
}