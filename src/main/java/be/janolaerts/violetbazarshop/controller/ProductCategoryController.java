package be.janolaerts.violetbazarshop.controller;

import be.janolaerts.violetbazarshop.entity.ProductCategory;
import be.janolaerts.violetbazarshop.services.ProductCategoryService_I;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/productcategories")
public class ProductCategoryController {

    private ProductCategoryService_I productCategoryService;

    @Autowired
    public ProductCategoryController(ProductCategoryService_I productCategoryService) {
        this.productCategoryService = productCategoryService;
    }

    @GetMapping(value="/{id:^\\d+$}", produces="application/json")
    public ResponseEntity<ProductCategory> getByIdHandler(@PathVariable("id") int id) {

        ProductCategory productCategory = productCategoryService.getProductCategoryById(id);
        return productCategory != null ? ResponseEntity.ok(productCategory) : ResponseEntity.badRequest().build();
    }

    @GetMapping(produces="application/json")
    public ResponseEntity<List<ProductCategory>> getAllHandler() {

        List<ProductCategory> productCategories = productCategoryService.getAllProductCategories();
        return ResponseEntity.ok(productCategories);
    }
}