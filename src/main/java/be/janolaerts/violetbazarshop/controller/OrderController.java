package be.janolaerts.violetbazarshop.controller;

import be.janolaerts.violetbazarshop.entity.Order;
import be.janolaerts.violetbazarshop.entity.Product;
import be.janolaerts.violetbazarshop.exception.ExceptionHandling;
import be.janolaerts.violetbazarshop.exception.domain.NotEnoughStockException;
import be.janolaerts.violetbazarshop.services.CartItemService_I;
import be.janolaerts.violetbazarshop.services.EmailService_I;
import be.janolaerts.violetbazarshop.services.OrderService_I;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;
import java.util.List;

@RestController
@RequestMapping("/orders")
public class OrderController extends ExceptionHandling {

    private OrderService_I orderService;
    private CartItemService_I cartItemService;
    private EmailService_I emailService;

    @Autowired
    public OrderController(OrderService_I orderService, CartItemService_I cartItemService, EmailService_I emailService) {
        this.orderService = orderService;
        this.cartItemService = cartItemService;
        this.emailService = emailService;
    }

    @GetMapping(path="/{id:^\\d+$}", produces="application/json")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public ResponseEntity<Order> getByIdHandler(@PathVariable("id") int id) {
        Order order = orderService.getOrderById(id);
        return order != null ? ResponseEntity.ok(order) : ResponseEntity.badRequest().build();
    }

    @GetMapping(path="/{email}", produces="application/json")
    @PreAuthorize("hasAuthority('ROLE_ADMIN') or #email == principal")
    public ResponseEntity<List<Order>> getByUserEmail(@PathVariable("email") String email) {
        List<Order> orders = orderService.getAllOrdersByUserEmail(email);
        return ResponseEntity.ok(orders);
    }

    @GetMapping(path="/getAll", produces="application/json")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public ResponseEntity<List<Order>> getAllHandler() {
        List<Order> orders = orderService.getAllOrders();
        return ResponseEntity.ok(orders);
    }

    @PostMapping(path="/place", params={"paymentId"}, consumes="application/json", produces="application/json")
    @PreAuthorize("#order.user.email == principal")
    public ResponseEntity<Order> postHandler(@RequestBody Order order, @RequestParam("paymentId") String paymentId) throws MessagingException {

        System.out.println("test");
        try {
            order = orderService.saveOrder(order);
            cartItemService.saveCartItemsByOrder(order);
            emailService.sendOrderAcceptedEmail(order);
        } catch (NotEnoughStockException ex) {
            emailService.sendPaymentOkButNotEnoughStockEmail(order, paymentId);
            order = null;
        }

        return order != null ? ResponseEntity.ok(order) : ResponseEntity.badRequest().build();
    }
}