package be.janolaerts.violetbazarshop.controller;

import be.janolaerts.violetbazarshop.domain.HttpResponse;
import be.janolaerts.violetbazarshop.entity.CartItem;
import be.janolaerts.violetbazarshop.entity.User;
import be.janolaerts.violetbazarshop.exception.ExceptionHandling;
import be.janolaerts.violetbazarshop.services.PaymentService_I;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationConfig;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.mercadopago.exceptions.MPException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;
import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/payment")
public class PaymentController extends ExceptionHandling {

    PaymentService_I paymentService;
    ObjectMapper mapper;

    @Autowired
    public PaymentController(PaymentService_I paymentService) {
        this.paymentService = paymentService;
        this.mapper = new ObjectMapper();
    }

    @PostMapping(consumes={MediaType.MULTIPART_FORM_DATA_VALUE}, produces="text/plain")
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<String> payHandler(@RequestParam("user") String userJsonString,
                                             @RequestParam("cartItems") String cartItemsJsonString) throws IOException, MPException {

        mapper.registerModule(new JavaTimeModule());
        User user = mapper.readValue(userJsonString, User.class);
        List<CartItem> cartItems = mapper.readValue(cartItemsJsonString, new TypeReference<>() {});
        return ResponseEntity.ok(paymentService.pay(user, cartItems));
    }

    @PostMapping(path="/cancel")
    @PreAuthorize("hasAuthority('ROLE_ADMIN') or #email == principal")
    public ResponseEntity<HttpResponse> cancelPayment(@RequestParam("firstName") String firstName,
                                                      @RequestParam("email") String email,
                                                      @RequestParam("payment_id") String paymentId) throws MPException, MessagingException {

        String message = paymentService.cancelPayment(firstName, email, paymentId);
        return response(HttpStatus.OK, message);
    }

    private ResponseEntity<HttpResponse> response(HttpStatus httpStatus, String message) {
        return new ResponseEntity<>(new HttpResponse(httpStatus.value(), httpStatus, httpStatus.getReasonPhrase(), message), httpStatus);
    }
}