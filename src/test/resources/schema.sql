CREATE TABLE IF NOT EXISTS product_categories (
     id              INT IDENTITY PRIMARY KEY,
     category_name   VARCHAR(255) NOT NULL
);

CREATE TABLE IF NOT EXISTS products (
    id              INT IDENTITY PRIMARY KEY,
    name            VARCHAR(64) NOT NULL,
    brand           VARCHAR(64) NOT NULL,
    description     VARCHAR(255) NOT NULL,
    price           DECIMAL(19,2) NOT NULL,
    image_url       VARCHAR(255) NOT NULL,
    stock           INT NOT NULL,
    category_id     INT NOT NULL,
    CONSTRAINT FK_products_product_categories FOREIGN KEY (category_id)
    REFERENCES product_categories(id) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS addresses (
  id            INT IDENTITY PRIMARY KEY,
  street        VARCHAR(255) NOT NULL,
  number        VARCHAR(255) NOT NULL,
  city          VARCHAR(255) NOT NULL,
  province      VARCHAR(255) NOT NULL,
  department    VARCHAR(255) NOT NULL
);

CREATE TABLE IF NOT EXISTS users (
  email                  VARCHAR(255) NOT NULL PRIMARY KEY,
  password               VARCHAR(255) NOT NULL,
  dni                    VARCHAR(8)   NOT NULL,
  firstNames             VARCHAR(255) NOT NULL,
  lastName_1             VARCHAR(255) NOT NULL,
  lastName_2             VARCHAR(255) NOT NULL,
  lastLoginDate          DATE DEFAULT NULL,
  lastLoginDateDisplay   DATE DEFAULT NULL,
  joinDate               DATE DEFAULT NULL,
  role                   VARCHAR(255) DEFAULT NULL,
  authorities            BLOB DEFAULT NULL,
  isNotLocked            BIT(1) DEFAULT NULL,
  isActive               BIT(1) DEFAULT NULL,
  address_id             INT NOT NULL,
  CONSTRAINT FK_users_addresses FOREIGN KEY (address_id)
  REFERENCES addresses(id) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS orders (
  id              INT IDENTITY PRIMARY KEY,
  placement_time  DATETIME NOT NULL,
  user_email      VARCHAR(255) NOT NULL,
  send_firstNames VARCHAR(255) NOT NULL,
  send_lastNames  VARCHAR(255) NOT NULL,
  address_id      INT NOT NULL,
  total_price     DECIMAL(19,2) NOT NULL,
  CONSTRAINT FK_orders_users FOREIGN KEY (user_email)
  REFERENCES users(email) ON DELETE CASCADE,
  CONSTRAINT FK_orders_addresses FOREIGN KEY (address_id)
  REFERENCES addresses(id) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS cart_items (
  id            INT IDENTITY PRIMARY KEY,
  name          VARCHAR(64) NOT NULL,
  price         DECIMAL(19,2) NOT NULL,
  image_url     VARCHAR(255) NOT NULL,
  stock         INT NOT NULL,
  quantity      INT NOT NULL,
  order_id      INT NOT NULL,
  CONSTRAINT FK_cart_items_orders FOREIGN KEY (order_id)
  REFERENCES orders(id) ON DELETE CASCADE
);