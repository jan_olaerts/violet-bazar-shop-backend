TRUNCATE TABLE products RESTART IDENTITY AND COMMIT;
TRUNCATE TABLE product_categories RESTART IDENTITY AND COMMIT;
TRUNCATE TABLE addresses RESTART IDENTITY AND COMMIT;
TRUNCATE TABLE users RESTART IDENTITY AND COMMIT;
TRUNCATE TABLE orders RESTART IDENTITY AND COMMIT;
TRUNCATE TABLE cart_items RESTART IDENTITY AND COMMIT;

INSERT INTO product_categories(id, category_name)
VALUES (1, 'SMARTPHONES'), (2, 'CARS'), (3, 'ELECTRONICS');

INSERT INTO products(id, name, brand, description, price, image_url, stock, category_id)
VALUES (1, 'iPhone 12 Pro', 'Apple', 'The latest smartphone from Apple', 1400.00, '/test', 878, 1),
       (2, 'Huawei P30', 'Huawei', 'A smartphone from Huawei', 249.99, '/test', 200, 2),
       (3, 'Tesla S', 'Tesla', 'An electric car from Tesla', 139999.99, '/test', 3, 2);

INSERT INTO addresses(id, street, number, city, province, department)
VALUES (1, 'Violet Street', '15 apartment 101', 'Violet Ville', 'Violet Province', 'Violet Department'),
       (2, 'Test Street', '1', 'Test City', 'Test Province', 'Test Department'),
       (3, 'Route du Lion', '1', 'Braine-Alleud', 'Brabant-Walon', 'Wallonia');

INSERT INTO users(email, password, dni, firstNames, lastName_1, lastName_2, lastLoginDate, lastLoginDateDisplay, joinDate, role, authorities, isNotLocked, isActive, address_id)
VALUES ('test_user@gmail.com', 'test', '12345678', 'Test', 'User', 'User', '2020-12-19', '2020-12-19', '2020-02-05', 'ROLE_USER', null, 1, 1, 2),
       ('violet_user@mail.com', 'violet', '87654321', 'Violet', 'User', 'User', '2020-12-12', '2020-12-12', '2019-05-14', 'ROLE_USER', null, 1, 1, 1),
       ('thalibau14@gmail.com', 'thalia', '46631303', 'Thalia Melissa', 'Bautista', 'Mejia', '2020-12-14', '2020-12-14', '2018-06-18', 'ROLE_ADMIN', null, 1, 1, 3);

INSERT INTO orders(id, placement_time, user_email, send_firstNames, send_lastNames, address_id, total_price)
VALUES (1, '2020-11-12 12:35:00', 'violet_user@mail.com', 'Violet', 'Bautista Mejia', 1, 141399.99),
       (2, '2020-09-14 15:25:00', 'thalibau14@gmail.com', 'Thalia', 'Bautista Mejia', 3, 249.99),
       (3, '2020-10-05 09:00:00', 'thalibau14@gmail.com', 'Jan', 'Olaerts Vanthoor', 2, 0);

INSERT INTO cart_items(id, name, price, image_url, stock, quantity, order_id)
VALUES (1, 'iPhone 12 Pro', 1400.00, '/test', 878, 6, 1),
       (2, 'Huawei P30', 249.99, '/test', 200, 15, 2),
       (3, 'Tesla S', 139999.99, '/test', 3, 1, 1);