package be.janolaerts.violetbazarshop.tools;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.transaction.Transactional;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@Transactional
class StringToolTest {

    @Test
    void validateEmail() {
        assertThrows(IllegalArgumentException.class, () -> StringTool.validateEmail(null));
        assertFalse(StringTool.validateEmail("test-user@violet.c"));
        assertTrue(StringTool.validateEmail("teSt-UsEr@violet.coM"));
    }

    @Test
    void capitalizeFirstLetters() {
        assertThrows(IllegalArgumentException.class, () -> StringTool.capitalizeFirstLetters(null));
        assertEquals("This Is A Test", StringTool.capitalizeFirstLetters("thIs iS a tEst"));
        assertEquals("This-Is-A-Test", StringTool.capitalizeFirstLetters("thIs-iS-a-tEst"));
        assertEquals("This-Is-A-Test-", StringTool.capitalizeFirstLetters("thIs-iS-a-tEst-"));
    }
}