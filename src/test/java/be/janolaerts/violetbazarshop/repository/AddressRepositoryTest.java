package be.janolaerts.violetbazarshop.repository;

import be.janolaerts.violetbazarshop.entity.Address;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.transaction.Transactional;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@Transactional
class AddressRepositoryTest {

    private AddressRepository ar;

    @Autowired
    public AddressRepositoryTest(AddressRepository ar) {
        this.ar = ar;
    }

    @Test
    void getById() {
        Address address = ar.getAddressById(3);
        assertNotNull(address);
        assertEquals("Route du Lion", address.getStreet());
    }

    @Test
    void findAll() {
        List<Address> addresses = ar.findAll();
        assertNotNull(addresses);
        assertEquals(3, addresses.size());
    }

    @Test
    void save() {
        Address address = new Address(0, "New Street", "New Number", "New City", "New Province", "New Department");
        address = ar.save(address);
        assertEquals(4, address.getId());
        assertEquals(4, ar.findAll().size());
    }

    @Test
    void deleteById() {
        ar.deleteById(1);
        assertEquals(2, ar.findAll().size());
    }
}