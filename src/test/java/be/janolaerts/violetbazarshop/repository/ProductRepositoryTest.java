package be.janolaerts.violetbazarshop.repository;

import be.janolaerts.violetbazarshop.entity.Product;
import be.janolaerts.violetbazarshop.entity.ProductCategory;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@Transactional
class ProductRepositoryTest {

    private ProductRepository pr;
    private ProductCategoryRepository pcr;

    @Autowired
    public ProductRepositoryTest(ProductRepository pr, ProductCategoryRepository pcr) {
        this.pr = pr;
        this.pcr = pcr;
    }

    @Test
    void getById() {
        Product product = pr.getProductById(2);
        assertNotNull(product);
        assertEquals("Huawei P30", product.getName());
    }

    @Test
    void findAll() {
        List<Product> products = pr.findAll();
        assertNotNull(products);
        assertEquals(3, products.size());
    }

    @Test
    void save() {
        ProductCategory category = pcr.getProductCategoryById(3);
        Product product = new Product(0, "Speaker", "JBL", "A stereo speaker from JBL", new BigDecimal("125.99"),
                                        "/test", 26, category);

        product = pr.save(product);
        assertEquals(4, product.getId());
        assertEquals(4, pr.findAll().size());
    }

    @Test
    void deleteById() {
        pr.deleteById(2);
        assertEquals(2, pr.findAll().size());
    }
}