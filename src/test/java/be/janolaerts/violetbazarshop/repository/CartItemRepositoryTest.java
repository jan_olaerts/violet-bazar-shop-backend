package be.janolaerts.violetbazarshop.repository;

import be.janolaerts.violetbazarshop.entity.CartItem;
import be.janolaerts.violetbazarshop.entity.Order;
import be.janolaerts.violetbazarshop.entity.Product;
import org.checkerframework.checker.units.qual.A;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.transaction.Transactional;

import java.math.BigDecimal;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@Transactional
class CartItemRepositoryTest {

    private CartItemRepository cir;
    private ProductRepository pr;
    private OrderRepository or;

    @Autowired
    public CartItemRepositoryTest(CartItemRepository cir, ProductRepository pr, OrderRepository or) {
        this.cir = cir;
        this.pr = pr;
        this.or = or;
    }

    @Test
    void getById() {
        CartItem cartItem = cir.getCartItemById(2);
        assertNotNull(cartItem);
        assertEquals("Huawei P30", cartItem.getName());
    }

    @Test
    void getCartItemsByOrder_IdOrderByName() {
        List<CartItem> cartItems = cir.getCartItemsByOrder_IdOrderByName(1);
        assertNotNull(cartItems);
        assertEquals(2, cartItems.size());
        assertEquals("Tesla S", cartItems.get(0).getName());
        assertEquals("iPhone 12 Pro", cartItems.get(1).getName());
    }

    @Test
    void findAllByOrder_Id() {
        List<CartItem> cartItems = cir.getCartItemsByOrder_IdOrderByName(1);
        assertNotNull(cartItems);
        assertEquals(2, cartItems.size());
    }

    @Test
    void findAll() {
        List<CartItem> cartItems = cir.findAll();
        assertNotNull(cartItems);
        assertEquals(3, cartItems.size());
    }

    @Test
    void save() {
        Product product = new Product();
        product.setId(4);
        product.setName("Test Product");
        product.setImageUrl("Test Image URL");
        product.setPrice(BigDecimal.valueOf(100.45));
        Order order = or.getOrderById(1);
        CartItem cartItem = new CartItem(product, 5, order);
        cartItem = cir.save(cartItem);
        assertEquals(4, cartItem.getId());
        assertEquals(4, cir.findAll().size());
    }

    @Test
    void deleteById() {
        cir.deleteById(3);
        assertEquals(2, cir.findAll().size());
    }
}