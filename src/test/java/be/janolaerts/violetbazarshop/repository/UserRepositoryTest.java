package be.janolaerts.violetbazarshop.repository;

import be.janolaerts.violetbazarshop.entity.Address;
import be.janolaerts.violetbazarshop.entity.Order;
import be.janolaerts.violetbazarshop.entity.User;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.transaction.Transactional;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@Transactional
class UserRepositoryTest {

    private UserRepository ur;
    private AddressRepository ar;

    @Autowired
    public UserRepositoryTest(UserRepository ur, AddressRepository ar) {
        this.ur = ur;
        this.ar = ar;
    }

    @Test
    void getByEmail() {
        User user = ur.findUserByEmail("violet_user@mail.com");
        assertNotNull(user);
        assertEquals("Violet", user.getFirstNames());
        assertEquals("User", user.getLastName_1());
    }

    @Test
    void getByDni() {
        User user = ur.findUserByDni("12345678");
        assertNotNull(user);
        assertEquals("Test", user.getFirstNames());
        assertEquals("User", user.getLastName_1());
    }

    @Test
    void findAll() {
        List<User> users = ur.findAll();
        assertNotNull(users);
        assertEquals(3, users.size());
    }

    @Test
    void save() {
        Address address = ar.getAddressById(3);
        User user = new User("test@user.com", "Test Password", "00000000","Test FirstNames", "Test LastName 1",
                "Test LastName 2", address, new Date(), new Date(), Date.from(Instant.parse("2020-06-18T00:00:00Z")), "ROLE_USER",
                new String[] {"user:read", "user:search"}, true, true, new ArrayList<>(List.of(new Order())));
        ur.save(user);
        assertEquals(4, ur.findAll().size());
    }

    @Test
    void deleteByEmail() {
        ur.deleteUserByEmail("test_user@gmail.com");
        assertEquals(2, ur.findAll().size());
    }
}