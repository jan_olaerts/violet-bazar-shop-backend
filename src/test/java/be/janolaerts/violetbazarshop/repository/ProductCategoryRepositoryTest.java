package be.janolaerts.violetbazarshop.repository;

import be.janolaerts.violetbazarshop.entity.ProductCategory;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@Transactional
class ProductCategoryRepositoryTest {

    private ProductCategoryRepository pcr;

    @Autowired
    public ProductCategoryRepositoryTest(ProductCategoryRepository pcr, ProductRepository pr) {
        this.pcr = pcr;
    }

    @Test
    void getById() {
        ProductCategory productCategory = pcr.getProductCategoryById(3);
        assertNotNull(productCategory);
        assertEquals("ELECTRONICS", productCategory.getCategoryName());
    }

    @Test
    void findAll() {
        List<ProductCategory> productCategories = pcr.findAll();
        assertNotNull(productCategories);
        assertEquals(3, productCategories.size());
    }

    @Test
    void save() {
        ProductCategory productCategory = new ProductCategory();
        productCategory.setCategoryName("GROCERIES");
        productCategory = pcr.save(productCategory);
        assertEquals(4, productCategory.getId());
        assertEquals(4, pcr.findAll().size());
    }

    @Test
    void deleteById() {
        pcr.deleteById(3);
        assertEquals(2, pcr.findAll().size());
    }
}