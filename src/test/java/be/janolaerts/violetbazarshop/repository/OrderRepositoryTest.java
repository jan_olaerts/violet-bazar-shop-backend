package be.janolaerts.violetbazarshop.repository;

import be.janolaerts.violetbazarshop.entity.CartItem;
import be.janolaerts.violetbazarshop.entity.Order;
import be.janolaerts.violetbazarshop.entity.User;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.transaction.Transactional;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@Transactional
class OrderRepositoryTest {

    OrderRepository or;
    UserRepository ur;

    @Autowired
    public OrderRepositoryTest(OrderRepository or, UserRepository ur) {
        this.or = or;
        this.ur = ur;
    }

    @Test
    void getById() {
        Order order = or.getOrderById(3);
        assertNotNull(order);
        assertEquals("2020-10-05 09:00", order.getPlacementTime().format(DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm")));
    }

    @Test
    void findAllByUser_Email() {
        List<Order> orders = or.getOrdersByUser_EmailOrderByPlacementTimeDesc("thalibau14@gmail.com");
        assertNotNull(orders);
        assertEquals(2, orders.size());
    }

    @Test
    void getAllOrdersByOrderByPlacementTimeDesc() {
        List<Order> orders = or.getAllOrdersByOrderByPlacementTimeDesc();
        assertNotNull(orders);
        assertTrue(orders.get(0).getPlacementTime().isAfter(orders.get(1).getPlacementTime()));
    }

    @Test
    void findAll() {
        List<Order> orders = or.findAll();
        assertNotNull(orders);
        assertEquals(3, orders.size());
    }

    @Test
    void save() {
        User user = ur.findUserByEmail("violet_user@mail.com");
        Order order = new Order(0, LocalDateTime.now(), user, user.getFirstNames(), user.getLastName_1() + " " + user.getLastName_2(), user.getAddress(),
        new ArrayList<>(List.of(new CartItem())), new BigDecimal(0));
        order = or.save(order);
        assertEquals(4, order.getId());
        assertEquals(4, or.findAll().size());
    }

    @Test
    void deleteById() {
        or.deleteById(1);
        assertEquals(2, or.findAll().size());
    }
}