package be.janolaerts.violetbazarshop.entity;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@Transactional
class OrderTest {

    private Order order;

    @BeforeEach
    private void makeOrder() {
        this.order = new Order();
    }

    @Test
    void setId() {
        assertThrows(IllegalArgumentException.class, () -> order.setId(-1));

        order.setId(5);
        assertEquals(5, order.getId());
    }

    @Test
    void setPlacementTime() {
        assertThrows(IllegalArgumentException.class, () -> order.setPlacementTime(null));
        assertThrows(IllegalArgumentException.class, () -> order.setPlacementTime(LocalDateTime.now().plusDays(1)));

        order.setPlacementTime(LocalDateTime.of(2020, 5, 20, 12, 0));
        assertEquals("2020-05-20 12:00", order.getPlacementTime().format(DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm")));
    }

    @Test
    void setUser() {
        assertThrows(IllegalArgumentException.class, () -> order.setUser(null));

        User user = new User();
        user.setFirstNames("Test User");
        order.setUser(user);
        assertEquals("Test User", order.getUser().getFirstNames());
    }

    @Test
    void setSend_FirstName() {
        assertThrows(IllegalArgumentException.class, () -> order.setSend_firstNames(null));
        assertThrows(IllegalArgumentException.class, () -> order.setSend_firstNames("a"));

        order.setSend_firstNames("test");
        assertEquals("Test", order.getSend_firstNames());

        order.setSend_firstNames("thalia melissa");
        assertEquals("Thalia Melissa", order.getSend_firstNames());

        order.setSend_firstNames("jAn-sMiTh");
        assertEquals("Jan-Smith", order.getSend_firstNames());
    }

    @Test
    void setSend_LastNames() {
        assertThrows(IllegalArgumentException.class, () -> order.setSend_lastNames(null));
        assertThrows(IllegalArgumentException.class, () -> order.setSend_lastNames("a"));

        order.setSend_lastNames("test");
        assertEquals("Test", order.getSend_lastNames());

        order.setSend_lastNames("bAutista meJia");
        assertEquals("Bautista Mejia", order.getSend_lastNames());
    }

    @Test
    void setSend_Address() {
        assertThrows(IllegalArgumentException.class, () -> order.setSend_address(null));

        Address address = new Address(505, "VioletStreet", "5, apt 101", "VioletVille", "Province of Violet","VioletDepartment");
        order.setSend_address(address);
        assertEquals(505, order.getSend_address().getId());
    }

    @Test
    void setCartItems() {
        assertThrows(IllegalArgumentException.class, () -> order.setCartItems(null));

        CartItem cartItem_1 = new CartItem();
        cartItem_1.setId(0);

        CartItem cartItem_2 = new CartItem();
        cartItem_2.setId(1);

        List<CartItem> cartItems = new ArrayList<>(List.of(
                cartItem_1,
                cartItem_2
        ));

        order.setCartItems(cartItems);
        assertEquals(2, order.getCartItems().size());
    }

    @Test
    void setTotalPrice() {
        assertThrows(IllegalArgumentException.class, () -> order.setTotalPrice(null));
        assertThrows(IllegalArgumentException.class, () -> order.setTotalPrice(new BigDecimal(-1)));

        order.setTotalPrice(new BigDecimal(120));
        assertEquals(120, order.getTotalPrice().doubleValue());
    }
}