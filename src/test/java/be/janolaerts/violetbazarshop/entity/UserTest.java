package be.janolaerts.violetbazarshop.entity;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.transaction.Transactional;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@Transactional
class UserTest {

    private User user;

    @BeforeEach
    private void makeUser() {
        this.user = new User();
    }

    @Test
    void setEmail() {
        assertThrows(IllegalArgumentException.class, () -> user.setEmail(null));
        assertThrows(IllegalArgumentException.class, () -> user.setEmail("test"));

        user.setEmail("test123@test.com");
        assertEquals("test123@test.com", user.getEmail());
    }

    @Test
    void setPassword() {
        assertThrows(IllegalArgumentException.class, () -> user.setPassword(null));
        assertThrows(IllegalArgumentException.class, () -> user.setPassword("a"));

        user.setPassword("test");
        assertEquals("test", user.getPassword());
    }

    @Test
    void setDni() {
        assertThrows(IllegalArgumentException.class, () -> user.setDni(null));
        assertThrows(IllegalArgumentException.class, () -> user.setDni("1234567"));
        assertThrows(IllegalArgumentException.class, () -> user.setDni("123456789"));
        assertThrows(IllegalArgumentException.class, () -> user.setDni("1234567a"));

        user.setDni("12345678");
        assertEquals("12345678", user.getDni());
    }

    @Test
    void setFirstName() {
        assertThrows(IllegalArgumentException.class, () -> user.setFirstNames(null));
        assertThrows(IllegalArgumentException.class, () -> user.setFirstNames("a"));

        user.setFirstNames("test");
        assertEquals("Test", user.getFirstNames());

        user.setFirstNames("thalia melissa");
        assertEquals("Thalia Melissa", user.getFirstNames());

        user.setFirstNames("jAn-sMiTh");
        assertEquals("Jan-Smith", user.getFirstNames());
    }

    @Test
    void setLastName_1() {
        assertThrows(IllegalArgumentException.class, () -> user.setLastName_1(null));
        assertThrows(IllegalArgumentException.class, () -> user.setLastName_1("a"));

        user.setLastName_1("test");
        assertEquals("Test", user.getLastName_1());

        user.setLastName_1("bAutista meJia");
        assertEquals("Bautista Mejia", user.getLastName_1());
    }

    @Test
    void setLastName_2() {
        assertThrows(IllegalArgumentException.class, () -> user.setLastName_2(null));
        assertThrows(IllegalArgumentException.class, () -> user.setLastName_2("a"));

        user.setLastName_2("test");
        assertEquals("Test", user.getLastName_2());

        user.setLastName_2("bAutista meJia");
        assertEquals("Bautista Mejia", user.getLastName_2());
    }

    @Test
    void setAddress() {
        assertThrows(IllegalArgumentException.class, () -> user.setAddress(null));

        Address address = new Address(505, "VioletStreet", "5, apt 101", "VioletVille", "Province of Violet", "VioletDepartment");
        user.setAddress(address);
        assertEquals(505, user.getAddress().getId());
    }

    @Test
    void setLastLoginDate() {
        user.setLastLoginDate(new Date());
        assertEquals(new Date(), user.getLastLoginDate());
    }

    @Test
    void setLastLoginDateDisplay() {
        user.setLastLoginDateDisplay(new Date());
        assertEquals(new Date(), user.getLastLoginDateDisplay());
    }

    @Test
    void setJoinDate() {
        assertThrows(IllegalArgumentException.class, () -> user.setJoinDate(new Date(new Date().getTime() + (1000 * 60 * 60 * 24))));

        user.setJoinDate(new Date());
        assertEquals(new Date(), user.getJoinDate());
    }

    @Test
    void setRole() {
        user.setRole("test");
        assertEquals("test", user.getRole());
    }

    @Test
    void setAuthorities() {
        user.setAuthorities(new String[] { "user:read", "user:create", "user:update" });
        assertEquals("[user:read, user:create, user:update]", Arrays.toString(user.getAuthorities()));
    }

    @Test
    void setNotLocked() {
        user.setNotLocked(true);
        assertTrue(user.isNotLocked());

        user.setNotLocked(false);
        assertFalse(user.isNotLocked());
    }

    @Test
    void setActive() {
        user.setActive(true);
        assertTrue(user.isActive());

        user.setActive(false);
        assertFalse(user.isActive());
    }

    @Test
    void setOrders() {
        List<Order> orders = new ArrayList<>(List.of(
                new Order(0, LocalDateTime.now(), user, "test", "test", new Address(), new ArrayList<>(List.of(new CartItem())), new BigDecimal(0)),
                new Order(1, LocalDateTime.now(), user, "test", "test", new Address(), new ArrayList<>(List.of(new CartItem())), new BigDecimal(0))
        ));
        user.setOrders(orders);
        assertEquals(2, user.getOrders().size());
    }
}