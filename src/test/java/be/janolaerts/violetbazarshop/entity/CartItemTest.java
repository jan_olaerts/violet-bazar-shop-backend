package be.janolaerts.violetbazarshop.entity;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.transaction.Transactional;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@Transactional
class CartItemTest {

    private CartItem cartItem;

    @BeforeEach
    private void makeCartItem() {
        this.cartItem = new CartItem();
    }

    @Test
    void setId() {
        assertThrows(IllegalArgumentException.class, () -> cartItem.setId(-1));

        cartItem.setId(569);
        assertEquals(569, cartItem.getId());
    }

    @Test
    void setName() {
        assertThrows(IllegalArgumentException.class, () -> cartItem.setName(null));
        assertThrows(IllegalArgumentException.class, () -> cartItem.setName("a"));

        cartItem.setName("CartItem Test");
        assertEquals("CartItem Test", cartItem.getName());
    }

    @Test
    void setImageUrl() {
        assertThrows(IllegalArgumentException.class, () -> cartItem.setImageUrl(null));

        cartItem.setImageUrl("/test_url");
        assertEquals("/test_url", cartItem.getImageUrl());
    }

    @Test
    void setPrice() {
        assertThrows(IllegalArgumentException.class, () -> cartItem.setPrice(null));
        assertThrows(IllegalArgumentException.class, () -> cartItem.setPrice(BigDecimal.valueOf(-0.1)));

        cartItem.setPrice(BigDecimal.valueOf(12.15));
        assertEquals(12.15, cartItem.getPrice().doubleValue());
    }

    @Test
    void setStock() {
        assertThrows(IllegalArgumentException.class, () -> cartItem.setStock(-1));

        cartItem.setStock(100);
        assertEquals(100, cartItem.getStock());
    }

    @Test
    void setQuantity() {
        assertThrows(IllegalArgumentException.class, () -> cartItem.setQuantity(0));
        assertThrows(IllegalArgumentException.class, () -> cartItem.setQuantity(-1));

        cartItem.setQuantity(158);
        assertEquals(158, cartItem.getQuantity());
    }

    @Test
    void setOrder() {
        assertThrows(IllegalArgumentException.class, () -> cartItem.setOrder(null));

        Order order = new Order(0, LocalDateTime.now(), new User(), "test", "test", new Address(), new ArrayList<>(List.of(new CartItem())), new BigDecimal(0));
        cartItem.setOrder(order);
        assertEquals(order, cartItem.getOrder());
    }
}