package be.janolaerts.violetbazarshop.entity;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.transaction.Transactional;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@Transactional
class AddressTest {

    private Address address;

    @BeforeEach
    private void makeAddress() {
        this.address = new Address();
    }

    @Test
    void setId() {
        assertThrows(IllegalArgumentException.class, () -> address.setId(-1));

        address.setId(569);
        assertEquals(569, address.getId());
    }

    @Test
    void setStreet() {
        assertThrows(IllegalArgumentException.class, () -> address.setStreet(null));
        assertThrows(IllegalArgumentException.class, () -> address.setStreet("a"));

        address.setStreet("teststreet");
        assertEquals("Teststreet", address.getStreet());
    }

    @Test
    void setNumber() {
        assertThrows(IllegalArgumentException.class, () -> address.setNumber(null));
        assertThrows(IllegalArgumentException.class, () -> address.setNumber("a"));

        address.setNumber("5 apartment 101");
        assertEquals("5 Apartment 101", address.getNumber());
    }

    @Test
    void setCity() {
        assertThrows(IllegalArgumentException.class, () -> address.setCity(null));
        assertThrows(IllegalArgumentException.class, () -> address.setCity("a"));

        address.setCity("herent");
        assertEquals("Herent", address.getCity());
    }

    @Test
    void setProvince() {
        assertThrows(IllegalArgumentException.class, () -> address.setProvince(null));
        assertThrows(IllegalArgumentException.class, () -> address.setProvince("a"));

        address.setProvince("vlaams-brabant");
        assertEquals("Vlaams-Brabant", address.getProvince());
    }

    @Test
    void setDepartment() {
        assertThrows(IllegalArgumentException.class, () -> address.setDepartment(null));
        assertThrows(IllegalArgumentException.class, () -> address.setDepartment("a"));

        address.setDepartment("san miguel");
        assertEquals("San Miguel", address.getDepartment());
    }
}