package be.janolaerts.violetbazarshop.entity;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@Transactional
class ProductCategoryTest {

    private ProductCategory productCategory;

    @BeforeEach
    void makeProductCategory() {
        this.productCategory = new ProductCategory();
    }

    @Test
    void setId() {
        assertThrows(IllegalArgumentException.class, () -> productCategory.setId(-1));

        productCategory.setId(20);
        assertEquals(20, productCategory.getId());
    }

    @Test
    void setCategoryName() {
        assertThrows(IllegalArgumentException.class, () -> productCategory.setCategoryName(null));
        assertThrows(IllegalArgumentException.class, () -> productCategory.setCategoryName("a"));

        productCategory.setCategoryName("test");
        assertEquals("test", productCategory.getCategoryName());
    }

    @Test
    void setProducts() {
        List<Product> products = new ArrayList<>(List.of(
                new Product(5, "iPhone 12", "Apple", "iPhone 12, the latest smartphone from Apple",
                        BigDecimal.valueOf(950.99f), "/testUrl", 198, productCategory),
                new Product(6, "Huawei P30", "Huawei", "Huawei P30, a smartphone from Huawei",
                        BigDecimal.valueOf(250.99f), "/testUrl", 50, productCategory)));

        productCategory.setProducts(products);
        assertEquals(2, productCategory.getProducts().size());
    }
}