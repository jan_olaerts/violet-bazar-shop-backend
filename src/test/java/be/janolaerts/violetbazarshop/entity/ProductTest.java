package be.janolaerts.violetbazarshop.entity;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@Transactional
class ProductTest {

    private Product product;
    private static final String NINETEEN_CHARS = "abcdefghijklmnopqrs";
    private static final String TWENTY_CHARS = "abcdefghijklmnopqrst";

    @BeforeEach
    private void makeProduct() {
        this.product = new Product();
    }

    @Test
    void setId() {
        assertThrows(IllegalArgumentException.class, () -> product.setId(-1));

        product.setId(20);
        assertEquals(20, product.getId());
    }

    @Test
    void setName() {
        assertThrows(IllegalArgumentException.class, () -> product.setName(null));
        assertThrows(IllegalArgumentException.class, () -> product.setName("a"));
        assertDoesNotThrow(() -> product.setName("ab"));

        product.setName("testProduct");
        assertEquals("testProduct", product.getName());
    }

    @Test
    void setBrand() {
        assertThrows(IllegalArgumentException.class, () -> product.setBrand(null));
        assertThrows(IllegalArgumentException.class, () -> product.setBrand("b"));
        assertDoesNotThrow(() -> product.setBrand("ab"));

        product.setBrand("testBrand");
        assertEquals("testBrand", product.getBrand());
    }

    @Test
    void setDescription() {
        assertThrows(IllegalArgumentException.class, () -> product.setDescription(null));
        assertThrows(IllegalArgumentException.class, () -> product.setDescription(NINETEEN_CHARS));
        assertDoesNotThrow(() -> product.setDescription(TWENTY_CHARS));

        product.setDescription("1a2b3c4d5e6f7g8h9i10");
        assertEquals("1a2b3c4d5e6f7g8h9i10", product.getDescription());
    }

    @Test
    void setPrice() {
        assertThrows(IllegalArgumentException.class, () -> product.setPrice(null));
        assertThrows(IllegalArgumentException.class, () -> product.setPrice(BigDecimal.valueOf(-0.1)));

        product.setPrice(BigDecimal.valueOf(12.15));
        assertEquals(12.15, product.getPrice().doubleValue());
    }

    @Test
    void setImageUrl() {
        assertThrows(IllegalArgumentException.class, () -> product.setImageUrl(null));

        product.setImageUrl("test/test");
        assertEquals("test/test", product.getImageUrl());
    }

    @Test
    void setStock() {
        assertThrows(IllegalArgumentException.class, () -> product.setStock(-1));

        product.setStock(145);
        assertEquals(145, product.getStock());
    }

    @Test
    void setCategory() {
        assertThrows(IllegalArgumentException.class, () -> product.setCategory(null));

        ProductCategory category = new ProductCategory();
        category.setId(685);

        product.setCategory(category);
        assertEquals(685, product.getCategory().getId());
    }
}