package be.janolaerts.violetbazarshop.services;

import be.janolaerts.violetbazarshop.entity.Address;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.transaction.Transactional;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@Transactional
class AddressServiceTest {

    private AddressService_I as;

    @Autowired
    public AddressServiceTest(AddressService_I as) {
        this.as = as;
    }

    @Test
    void getAddressById() {
        Address address = as.getAddressById(3);
        assertNotNull(address);
        assertEquals("Route du Lion", address.getStreet());
    }

    @Test
    void getAllAddresses() {
        List<Address> addresses = as.getAllAddresses();
        assertNotNull(addresses);
        assertEquals(3, addresses.size());
    }

    @Test
    void saveAddress() throws Exception {
        Address address;

        assertNull(as.saveAddress(null));
        assertEquals(3, as.getAllAddresses().size());

        address = new Address(1, "New Street", "New Number", "New City", "New Province", "New Department");
        assertNull(as.saveAddress(address));
        assertEquals(3, as.getAllAddresses().size());

        address = new Address(0, "New Street", "New Number", "New City", "New Province", "New Department");
        address = as.saveAddress(address);

//        assertEquals(4, address.getId());
        assertEquals(4, as.getAllAddresses().size());
    }

    @Test
    void deleteAddress() throws Exception {
        as.deleteAddress(1);
        assertEquals(2, as.getAllAddresses().size());
    }
}