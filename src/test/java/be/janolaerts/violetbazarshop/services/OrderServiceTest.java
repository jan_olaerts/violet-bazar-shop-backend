package be.janolaerts.violetbazarshop.services;

import be.janolaerts.violetbazarshop.entity.CartItem;
import be.janolaerts.violetbazarshop.entity.Order;
import be.janolaerts.violetbazarshop.entity.User;
import be.janolaerts.violetbazarshop.exception.domain.NotEnoughStockException;
import be.janolaerts.violetbazarshop.repository.CartItemRepository;
import be.janolaerts.violetbazarshop.repository.OrderRepository;
import be.janolaerts.violetbazarshop.repository.ProductRepository;
import be.janolaerts.violetbazarshop.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.transaction.Transactional;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@Transactional
class OrderServiceTest {

    private OrderService_I os;
    private UserRepository ur;
    private CartItemRepository cir;
    private ProductRepository pr;

    @Autowired
    public OrderServiceTest(OrderService_I os, UserRepository ur, CartItemRepository cir, ProductRepository pr) {
        this.os = os;
        this.ur = ur;
        this.cir = cir;
        this.pr = pr;
    }

    @Test
    void getOrderById() {
        Order order = os.getOrderById(3);
        assertNotNull(order);
        assertEquals("2020-10-05 09:00", order.getPlacementTime().format(DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm")));
    }

    @Test
    void getAllOrdersByUserEmail() {
        List<Order> orders = os.getAllOrdersByUserEmail("thalibau14@gmail.com");
        assertNotNull(orders);
        assertEquals(2, orders.size());
    }

    @Test
    void getAllOrders() {
        List<Order> orders = os.getAllOrders();
        assertNotNull(orders);
        assertEquals(3, orders.size());
    }

    @Test
    void saveOrder() throws NotEnoughStockException {

        assertNull(os.saveOrder(null));

        Order order;
        User user = ur.findUserByEmail("violet_user@mail.com");
        List<CartItem> cartItems = cir.findAll();

        order = new Order(1, LocalDateTime.now(), user, user.getFirstNames(), user.getLastName_1() + " " + user.getLastName_2(), user.getAddress(),
                cartItems, new BigDecimal("141399.99"));

        assertNull(os.saveOrder(order));

        order = new Order(0, LocalDateTime.now(), user, user.getFirstNames(), user.getLastName_1() + " " + user.getLastName_2(), user.getAddress(),
                List.of(new CartItem(pr.getProductById(3), 4, os.getOrderById(1))), new BigDecimal("141399.99"));

        Order finalOrder = order;
        assertThrows(NotEnoughStockException.class, () -> os.saveOrder(finalOrder));

        order = new Order(0, LocalDateTime.now(), user, user.getFirstNames(), user.getLastName_1() + " " + user.getLastName_2(), user.getAddress(),
                cartItems, new BigDecimal("141399.99"));

        order = os.saveOrder(order);

        assertEquals(872, pr.getProductById(1).getStock());
        assertEquals(185, pr.getProductById(2).getStock());
        assertEquals(2, pr.getProductById(3).getStock());

//        assertEquals(4, order.getId());
        assertEquals(2, os.getAllOrdersByUserEmail("violet_user@mail.com").size());
    }
}