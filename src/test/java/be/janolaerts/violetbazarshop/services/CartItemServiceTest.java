package be.janolaerts.violetbazarshop.services;

import be.janolaerts.violetbazarshop.entity.CartItem;
import be.janolaerts.violetbazarshop.entity.Order;
import be.janolaerts.violetbazarshop.entity.Product;
import be.janolaerts.violetbazarshop.repository.OrderRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.transaction.Transactional;

import java.math.BigDecimal;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@Transactional
class CartItemServiceTest {

    CartItemService_I cis;
    OrderRepository or;

    @Autowired
    public CartItemServiceTest(CartItemService_I cis, OrderRepository or) {
        this.cis = cis;
        this.or = or;
    }

    @Test
    void getCartItemById() {
        CartItem cartItem = cis.getCartItemById(2);
        assertNotNull(cartItem);
        assertEquals("Huawei P30", cartItem.getName());
    }

    @Test
    void getAllCartItems() {
        List<CartItem> cartItems = cis.getAllCartItems();
        assertNotNull(cartItems);
        assertEquals(3, cartItems.size());
    }

    @Test
    void getAllCartItemsByOrderId() {
        List<CartItem> cartItems = cis.getAllCartItemsByOrderId(1);
        assertNotNull(cartItems);
        assertEquals(2, cartItems.size());
    }

    @Test
    void saveCartItem() {
        assertNull(cis.saveCartItem(null));

        Product product = new Product();
        product.setId(4);
        product.setName("Test Product");
        product.setImageUrl("Test Image URL");
        product.setPrice(BigDecimal.valueOf(100.45));
        Order order = or.getOrderById(1);
        CartItem cartItem = new CartItem(product, 5, order);
        cartItem = cis.saveCartItem(cartItem);
//        assertEquals(4, cartItem.getId());
        assertEquals(4, cis.getAllCartItems().size());
    }

    @Test
    void deleteCartItem() {
        cis.deleteCartItem(3);
        assertEquals(2, cis.getAllCartItems().size());
    }
}