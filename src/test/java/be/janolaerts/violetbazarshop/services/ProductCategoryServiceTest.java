package be.janolaerts.violetbazarshop.services;

import be.janolaerts.violetbazarshop.entity.ProductCategory;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.transaction.Transactional;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@Transactional
class ProductCategoryServiceTest {

    private ProductCategoryService_I pcs;

    @Autowired
    public ProductCategoryServiceTest(ProductCategoryService_I pcs) {
        this.pcs = pcs;
    }

    @Test
    void getProductCategoryById() {
        ProductCategory productCategory = pcs.getProductCategoryById(3);
        assertNotNull(productCategory);
        assertEquals("ELECTRONICS", productCategory.getCategoryName());
    }

    @Test
    void getAllProductCategories() {
        List<ProductCategory> productCategories = pcs.getAllProductCategories();
        assertNotNull(productCategories);
        assertEquals(3, productCategories.size());
    }

    @Test
    void saveProductCategory() {
        ProductCategory productCategory = new ProductCategory();
        productCategory.setCategoryName("GROCERIES");
        productCategory = pcs.saveProductCategory(productCategory);
//        assertEquals(4, productCategory.getId());
        assertEquals(4, pcs.getAllProductCategories().size());
    }
}