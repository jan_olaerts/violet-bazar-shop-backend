package be.janolaerts.violetbazarshop.domain;

import be.janolaerts.violetbazarshop.entity.Order;
import be.janolaerts.violetbazarshop.entity.User;
import be.janolaerts.violetbazarshop.repository.AddressRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.transaction.Transactional;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@Transactional
class UserPrincipalTest {

    private UserPrincipal userPrincipal;
    private AddressRepository ar;

    @Autowired
    public UserPrincipalTest(AddressRepository ar) {
        this.ar = ar;
    }

    @BeforeEach
    void makeUserPrincipal() {
        User user = new User("test@user.com", "Test Password", "00000000", "Test FirstName 1", "Test FirstName 2", "Test LastName", ar.getAddressById(3), new Date(),
                new Date(), Date.from(Instant.parse("2020-06-18T00:00:00Z")), "ROLE_USER", new String[] {"user:read", "user:search"}, true, true, new ArrayList<>(List.of(new Order())));

        this.userPrincipal = new UserPrincipal(user);
    }

    @Test
    void getAuthorities() {
        assertEquals("[user:read, user:search]", userPrincipal.getAuthorities().toString());
    }

    @Test
    void getPassword() {
        assertEquals("Test Password", userPrincipal.getPassword());
    }

    @Test
    void getUsername() {
        assertEquals("test@user.com", userPrincipal.getUsername());
    }

    @Test
    void isAccountNonExpired() {
        assertTrue(userPrincipal.isAccountNonExpired());
    }

    @Test
    void isAccountNonLocked() {
        assertTrue(userPrincipal.isAccountNonLocked());
    }

    @Test
    void isCredentialsNonExpired() {
        assertTrue(userPrincipal.isCredentialsNonExpired());
    }

    @Test
    void isEnabled() {
        assertTrue(userPrincipal.isEnabled());
    }
}