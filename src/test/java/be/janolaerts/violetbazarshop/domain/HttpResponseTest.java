package be.janolaerts.violetbazarshop.domain;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.transaction.Transactional;

import java.time.Instant;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@Transactional
class HttpResponseTest {

    private HttpResponse httpResponse;

    @BeforeEach
    void makeHttpResponse() {
        this.httpResponse = new HttpResponse();
    }

    @Test
    void setHttpStatusCode() {
        assertThrows(IllegalArgumentException.class, () -> httpResponse.setHttpStatusCode(99));
        assertThrows(IllegalArgumentException.class, () -> httpResponse.setHttpStatusCode(600));

        httpResponse.setHttpStatusCode(101);
        assertEquals(101, httpResponse.getHttpStatusCode());
    }

    @Test
    void setHttpStatus() {
        assertThrows(IllegalArgumentException.class, () -> httpResponse.setHttpStatus(null));

        httpResponse.setHttpStatus(HttpStatus.OK);
        assertEquals(HttpStatus.OK, httpResponse.getHttpStatus());
    }

    @Test
    void setReason() {
        assertThrows(IllegalArgumentException.class, () -> httpResponse.setReason(null));
        assertThrows(IllegalArgumentException.class, () -> httpResponse.setReason("a"));

        httpResponse.setReason("This is a reason");
        assertEquals("This is a reason", httpResponse.getReason());
    }

    @Test
    void setMessage() {
        assertThrows(IllegalArgumentException.class, () -> httpResponse.setMessage(null));
        assertThrows(IllegalArgumentException.class, () -> httpResponse.setMessage("a"));

        httpResponse.setMessage("This is a message");
        assertEquals("This is a message", httpResponse.getMessage());
    }

    @Test
    void setTimeStamp() {
        assertThrows(IllegalArgumentException.class, () -> httpResponse.setTimeStamp(null));
        assertThrows(IllegalArgumentException.class, () -> httpResponse.setTimeStamp(new Date(new Date().getTime() + (1000 * 60 * 60 * 24))));

        httpResponse.setTimeStamp(Date.from(Instant.parse("2020-11-12T00:00:00Z")));
        assertEquals(Date.from(Instant.parse("2020-11-12T00:00:00Z")), httpResponse.getTimeStamp());
    }
}